<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Free Ladder - Auto Update</title>
    <style>
        body {
            width: 80%;
            min-width: 960px;
            margin: 0 auto;
            padding: 0 5%;
        }

        table {
            text-align: center;
            width: 100%;
            border-collapse: collapse;
            border-spacing: 0;
            word-break: break-all;
            word-wrap: break-word;
        }

        table th {
            font-weight: bold;
            background: #efefef;
            padding: 6px;
            border: 1px solid #dfdfdf;
            font-size: 16px;
        }

        table td {
            border: 1px solid #dfdfdf;
            padding: 6px;
            font-size: 14px;
        }

        input {
            width: 100%;
            border: none;
            outline: none;
            font-size: 12px;
        }
    </style>
</head>
<body>
<?php
$ssr = file_get_contents('ssr.json');
$ssr = json_decode($ssr, true);
$end = strtotime(date("Y-m-d"), time()) + 60 * 60 * 24;

if ($ssr['update'] < $end) {
    $url = "http://www.xwcuc.com/ziyuanfenxiang/mianfeijiaocheng";
    $pattern = '/<h2><a href="(.*?)" title="(.*?)" target="_blank">(.*?)<\/a><\/h2>/';
    $link = preg($url, $pattern, 1);

    if ($link) {
        $pattern = '/ssr:\/\/\w+/';
        $ssr = preg($link, $pattern, 0, 1);

        if ($ssr) {
            $ssr = ['ssr' => $ssr, 'update' => $end];
            file_put_contents('ssr.json', json_encode($ssr));
        } else {
            echo '<h3 style="color: #F40">Error: No SSR</h3>';
        }
    } else {
        echo '<h3 style="color: #F40">Error: No Page</h3>';
    }
}

function preg($url, $pattern, $index, $mode = 0)
{
    $dom = file_get_contents($url);
    if ($mode) {
        preg_match_all($pattern, $dom, $res);
    } else {
        preg_match($pattern, $dom, $res);
    }
    if (!empty($res)) {
        $res = $res[$index];
    } else {
        $res = false;
    }

    return $res;
}

?>
<h1 style="font-size: 40px;text-align: center">Free Ladder</h1>

<h1>SSR Link</h1>
<table>
    <tr>
        <th style="width: 5%">#</th>
        <th style="width: 75%">SSR</th>
        <th style="width: 10%">Update</th>
        <th style="width: 10%">Option</th>
    </tr>
    <?php
    $update = date('Y/m/d', $ssr['update']);

    $id = 0;
    foreach ($ssr['ssr'] as $value) {
        $id++;
        echo '<tr><td>' . $id . '</td><td><input id="ssr_val' . $id . '" value="' . $value . '"></td><td>' . $update . '</td><td><button onclick="copy_ssr(' . $id . ')">Copy</button></td></tr>';
    }
    ?>
</table>

<h1>Attention</h1>
<div>
    <p>To help friends search for useful documents, they can use the following SSR to skip the GFW;</p>
    <p>But please note that all consequences arising from using SSR to bypass the firewall are the responsibility of the
        user!</p>
    <p>This website does not bear any responsibility.</p>
</div>

<h2>ShadowsocksR</h2>
<div>
    Click To Download: <a href="https://cdn.lifanko.cn/tools/ShadowsocksR-win-4.9.0.zip">https://cdn.lifanko.cn/tools/ShadowsocksR-win-4.9.0.zip</a>
</div>
<p style="font-size: 12px;margin-top: 5%;text-align: right;">
    © 2016-2020 lifanko
    <a href="http://www.beian.miit.gov.cn/" target="_blank"
       style="text-decoration: none;color: #333">豫ICP备16040860号</a>
</p>
<script>
    function copy_ssr(id) {
        document.getElementById("ssr_val" + id).select();
        document.execCommand("Copy");
    }
</script>
</body>
</html>
