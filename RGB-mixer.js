var r = 255;
var g = 0;
var b = 0;

var stage = 1;

var i;

for (i = 0; i < 255 * 6 + 1; i++) {
    console.log(r + '-' + g + '-' + b);

    if (stage === 1) {
        if (++b === 255) {
            stage = 2;
        }
    } else if (stage === 2) {
        if (--r === 0) {
            stage = 3;
        }
    } else if (stage === 3) {
        if (++g === 255) {
            stage = 4;
        }
    } else if (stage === 4) {
        if (--b === 0) {
            stage = 5;
        }
    } else if (stage === 5) {
        if (++r === 255) {
            stage = 6;
        }
    } else if (stage === 6) {
        if (--g === 0) {
            stage = 1;
        }
    }
}
