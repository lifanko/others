#ifndef __MAIN_H
#define __MAIN_H	 
#include "sys.h"
#include <stdint.h>
/*----------------------------------------*/


#include "key.h"
#include "led.h"      //ָʾ��
#include "lock.h"

#include "delay.h"
#include "timer.h"
#include "timer4.h"
#include "exti.h"

#include "usart1.h"   //motor control card
#include "usart2.h"   //RS232
#include "uart4.h"	  

#include "uart5.h"		//console

//#include "dma.h"
//#include "dma.h"
//#include "adc.h"

//#include "crc16.h"    //CRC16У��
//#include "stmflash.h"

#include "stdio.h"	
#include "string.h" 
#include "stdlib.h"

#include "Host.h"
#include "Motor_Control.h"
#include "Bar_Code_Scan.h"
#include "dht11.h"
#include "wdg.h"
/*----------------------------------------*/


void Exti_key(u8 Key_Value);
void Exti_door(u8 Door_Status);
void tip(uint8_t time);
void self_check(void);

#endif
