/*----------------------------------------*/
// * @brief    :
// * @details  :
// * @author   : BigStrong
// * @date     : 2018.09.13
// * @version  : V1.0
/*----------------------------------------*/
#include "main.h"

#define Status_Machine_Standby        0x00   //设备待机
#define Status_Machine_On             0x01   //设备开机
#define Status_Machine_Off            0x02   //设备关机
#define Status_Machine_Maintenance    0x04   //设备维护
#define Status_Machine_Malfunction    0x08   //设备故障中
#define Status_Machine_Selfcheck      0x10   //设备自检中
#define Status_Machine_Create_table   0x20   //图书建表中
#define Status_Machine_Borrow_Book    0x40   //借书中
#define Status_Machine_Return_Book    0x80   //还书中

#define  ON    0x01
#define  OFF   0x00

void Exti_key(u8 Key_Value);
void Exti_door(u8 Door_Status);


#define SEND_BUF_SIZE 8  //发送数据长度,最好等于sizeof(TEXT_TO_SEND)+2的整数倍.?????????????????
u8 SendBuff[SEND_BUF_SIZE]; //发送数据缓冲区
const u8 TEXT_TO_SEND[] = {"ALIENTEK Elite STM32F1 DMA 串口实验"};
extern u8 Machine_Status_Respond[];
extern char Motor1_Locate_Inquire_Array[30];    //马达1位置查询
extern vu8 Bar_Code_Temp[];
extern char Motor_Increment_Move_Array[];      //绝对移动发送帧
extern u8 Return_Book_Inform[];
extern u8 Lock_Status_Respond[];
extern u8 ERC_Inform[];
extern u8 Borrow_Book_Inform[];
extern void EPC_Read(void);
extern vu8 Global_Module_fault_Code;
extern vu8 Global_Machine_Status;//全局机器状态
extern vu8 Fault_Code_Refresh;//故障代码刷新标志

extern uint8_t Get_Lock_Status(void);

extern u8 DOOR_Action_Inform[];//柜门开关通知应帧
extern char Motor_Increment_Move_Array[];     //增量移动发送帧
extern char Motor1_Slow_Stop_Array[];        //马达1慢停
//extern u8 Sum_Check(u8 arry[],u16 n);

#define M 10 //AD滑动平均次数
u16 value[M];//AD采样一维数组

u32 adcx = 0;

vu8 ATUO_Control = 1;

u8 Human_Det_Status = 0;
u8 Human_Det_Status_temp = 0;
u8 Door_Status = 0;

u8 Key_Value = 0;//按键值

vu8 Lock_Action_Flag = 0;//锁动作标志

u8 Temperature = 0;
u8 Relative_Humidity = 0;
uint8_t Time_Temp_Hum = 10;

uint8_t Second = 0;
uint8_t Minute = 0;
uint16_t Hour   = 0;
uint8_t Second_temp = 0;

vu8 Motor_CTL_Card_rst = 1; //电机控制卡重启标志

vu16 led0pwmval = 170;

char console_val[60];

u8 block = 0;

u8 send_close = 0;

uint8_t uptime = 1;
uint8_t FAN_status = 0;

extern uint8_t Lock_Status;

uint8_t block_exti = 0;

uint8_t i = 0;

/*---------------------------------------------------------------------------------*/
//#define FLASH_SAVE_ADDR  0X08009000   //设置FLASH 保存地址(必须为偶数，且其值要大于本代码所占用FLASH的大小+0X08000000)
//
////设置FLASH 保存地址(必须为偶数，且其值要大于本代码所占用FLASH的大小+0X08000000)
//*----------------------------------------------------------------*/

int main(void)
{
  vu16 Module_fault_temp = 0;

  delay_init();        //延时函数初始化
  KEY_Init();
  LED_Init();        //初始化与LED连接的硬件接口
  BUZZER_Init();
  FAN_Init();      //风扇初始化
  Relay_Init();    //继电器初始化
  GPIO_Tunnel_Select();
  LOCK_Init();
	
	self_check();

  NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);// 设置中断优先级分组2
  TIM3_Int_Init(9999, 7199); //10Khz的计数频率，计数到1000为100ms/10Hz
  TIM4_PWM_Init(899, 0); //不分频。PWM频率=72000000/900=80Khz

  DHT11_Init();
  //控制卡通信口初始化
  RS485_init();
  uart1_init(9600);  //串口初始化为9600
  //扫码模块通信口初始化
  uart4_init(9600);
  // Console Init
  uart5_init(115200);

  Log("System Start");

  EXTIX_Init();
  delay_ms(200);
  Motor_Contol_init();//电机控制卡参数初始化
  delay_ms(200);
  TIM_SetCompare2(TIM4, led0pwmval);

  IWDG_Init(6, 625);    //分频数为256,重载值为625,溢出时间为4s

  Motor_Turntable_Rst();//上电自动回原点

  uart2_init(9600);     //串口初始化为9600

  for (i = 0; i < 3; i++) {
    bee(100);
  }

  while (1)
  {
    //检测设备状态,在待机状态下才进行数据解析；
    Data_Analysis();
    //按键处理函数
    Exti_key(Key_Value);
    //根据温湿度 自动控制风扇
    if (ATUO_Control) {
      if (uptime || (Second % Time_Temp_Hum == 0 && Second_temp != Second)) {
        Second_temp = Second;

        DHT11_Read_Data(&Temperature, &Relative_Humidity);

        if (uptime) {
          uptime = 0;
          sprintf(console_val, "Uptime:%dDay %dHour %dMin %dSec,T=%ddeg,H=%d%%", Hour / 24, Hour % 24, Minute, Second, Temperature, Relative_Humidity);
          Log(console_val);
        }

        if (Temperature > 40 || Relative_Humidity > 50) {
          if (FAN_status == 0) {
            Log("FANs Turn On");
            FAN1 = ON;
            FAN2 = ON;
            FAN_status = 1;
          }
        } else {
          if (FAN_status == 1) {
            Log("FANs Turn Off");
            FAN1 = OFF;
            FAN2 = OFF;
            FAN_status = 0;
          }
        }

        // Auto Restart: Uptime enough and standby status and usart1 no msg for 30s at least
        if (0 && Global_Machine_Status == 0x00) {
          Log("Runtime too long and no task now, Waiting WDG to restart...");

          block = 1;
          while (1);
        }
      }
    }

    //若查询到设备某部分故障，立即上报
    if (Fault_Code_Refresh) //10S刷新
    {
      Fault_Code_Refresh = 0;

      if (Global_Module_fault_Code) //全局模块故障标志
      {
        if (Global_Machine_Status == Status_Machine_Standby)
        {
          Global_Machine_Status = Status_Machine_Malfunction;
        }
        if (Global_Module_fault_Code != Module_fault_temp) //本次故障代码与上次相异
        {
          Module_fault_temp = Global_Module_fault_Code;
          tip(20);

          // Inform host if lock perform error
          //          ERC_Inform[5] = Global_Module_fault_Code >> 8;//故障码高字节
          //          ERC_Inform[6] = Global_Module_fault_Code;//故障码低字节
          //          ERC_Inform[7] =  Sum_Check(ERC_Inform, (ERC_Inform[3] << 8) + ERC_Inform[4]);
          //          USART_SendString_User(USART2, ERC_Inform, 9);
        }
      } else {
        if (Global_Machine_Status == Status_Machine_Malfunction)
        {
          Global_Machine_Status = Status_Machine_Standby;
        }
      }
    }

    //只要锁状态改变，均上传锁状态标志
    if (Lock_Action_Flag) {
      // Block lock signal for 500ms
      if (block_exti) {
        tip(50);
        delay_ms(400);
        block_exti = 0;
        Log("Block Lock Siginal for 500ms");
      } else {
        if (Global_Machine_Status & Status_Machine_Borrow_Book) //借书中
        {
          Lock_Status_Respond[2] = 0x44;
        } else if (Global_Machine_Status & Status_Machine_Return_Book) //还书中
        {
          Lock_Status_Respond[2] = 0x55;
        } else if (Global_Machine_Status & Status_Machine_Selfcheck) //设备自检中
        {
          Lock_Status_Respond[2] = 0x22;
        } else {
          Lock_Status_Respond[2] = 0xEE;//异常情况
        }

        Lock_Status_Respond[5] = 0x00;

        // Query Lock Status
        if (Lock_Action_Flag == 0x54) {
          Lock_Status = 0x00;
        } else if (Lock_Action_Flag < 0x50) {
          Lock_Status = Get_Lock_Status();
        }

        sprintf(console_val, "Report Lock Status:%x, Lock_Action_Flag:%x, task:%x", Lock_Status, Lock_Action_Flag, Lock_Status_Respond[2]);
        Log(console_val);

        Lock_Status_Respond[6] = Lock_Status;
        Lock_Status_Respond[7] = Sum_Check(Lock_Status_Respond, (Lock_Status_Respond[3] << 8) + Lock_Status_Respond[4]);
        USART_SendString_User(USART2, Lock_Status_Respond, 9); //上报当前锁状态帧

        if (Global_Machine_Status & Status_Machine_Borrow_Book) //借书中
        {
          //判断锁关闭
          if (Lock_Status == 0x00) {
            Borrow_Book_Finish();  //完成借书
          }
        }
      }

      Lock_Action_Flag = 0;
    }
  }
}

void self_check(void) {
	// Read check button
	while(!KEY_check){
		bee(200-i*20);
		
		if(i++ == 8){
			// Lock x5
			for(i=1;i<=5;i++) {
				delay_ms(500);
				
				lock_perform(i);
			}
			
			delay_ms(500);
			
			// Fan x2
			Buzzer = 1;
			FAN1 = 1;
			delay_ms(100);
			FAN1 = 0;
			Buzzer = 0;
			delay_ms(500);
			
			Buzzer = 1;
			FAN2 = 1;
			delay_ms(100);
			FAN2 = 0;
			Buzzer = 0;
			
			delay_ms(500);
			
			// Relay x1
			Buzzer = 1;
			Relay = 1;
			delay_ms(500);
			Relay = 0;
			Buzzer = 0;
			
			break;
		}
	}
	
	if(!i){
		bee(200);
	}
}

void tip(uint8_t time) {
  Buzzer = 1;
  delay_ms(time);
  Buzzer = 0;
  delay_ms(time);
}

void Exti_key(u8 Key_Value)
{
  static u8 key_value_temp = 0;

  if (Key_Value != key_value_temp)
  {
    sprintf(console_val, "Key:%d, Temp:%d", Key_Value, key_value_temp);
    Log(console_val);

    key_value_temp = Key_Value;
    switch (Key_Value)
    {
      case KEY_CLOCKWISE_DOWN://顺时针
        Motor_Control_String(Motor_Increment_Move_Array, Motor_Increment_Move, Motor_Turntable, 12000); //组成转盘转动命令
        SendCmd(Motor_Increment_Move_Array, "0", 100); //发送指令
        break;
      case KEY_CLOCKWISE_UP://停
        USART1_Clear();
        memset(Motor1_Slow_Stop_Array, 0, 30);
        strcpy(Motor1_Slow_Stop_Array, Motor1_Slow_Stop);
        USART1_Write(USART1, (unsigned char *)Motor1_Slow_Stop_Array, strlen((const char *)Motor1_Slow_Stop_Array));
        break;
      case KEY_ANTICLOCKWISE_DOWN://逆时针
        Motor_Control_String(Motor_Increment_Move_Array, Motor_Increment_Move, Motor_Turntable, -12000); //组成转盘转动命令
        SendCmd(Motor_Increment_Move_Array, "0", 100);//发送指令
        break;
      case KEY_ANTICLOCKWISE_UP://停
        USART1_Clear();
        memset(Motor1_Slow_Stop_Array, 0, 30);
        strcpy(Motor1_Slow_Stop_Array, Motor1_Slow_Stop);
        USART1_Write(USART1, (unsigned char *)Motor1_Slow_Stop_Array, strlen((const char *)Motor1_Slow_Stop_Array));
        break;
      case KEY_RESET_DOWN:
        break;
      case KEY_RESET_UP://回原点
        Buzzer = 1;
        delay_ms(100);
        Buzzer = 0;
        delay_ms(100);
        Motor_Turntable_Rst();
        break;
      default: break;
    }
  }
  Key_Value = 0;
}
