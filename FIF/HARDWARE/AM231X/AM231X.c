#include "AM231X.h"


unsigned char IIC_TX_Buffer[]={0x03,0x00,0x04}; //读温湿度命令（无CRC校验）
unsigned char IIC_RX_Buffer[IIC_RX_Length] = {0x00};//读回的温湿度
unsigned char WR_Flag;
float Tmp1,Tmp2;

//***************************************************
u8 WriteNByte(unsigned char sla,unsigned char *s,unsigned char n)
{
   unsigned char i;
   
   AMIIC_Start();  //启动I2C
   AMIIC_Send_Byte(sla);//发送器件地址
  /* if(!IIC_Write_Ack())
   {	
      WR_Flag = 1;
			return 0;
   }*/
   for(i=0;i<n;i++)//写入8字节数据
   {
      AMIIC_Send_Byte(*(s+i));
			/*if(!IIC_Write_Ack())
			{
				WR_Flag = 1;
				return(0);
			}*/
   }
   AMIIC_Stop();
   return 1;
}
u8 ReadNByte(unsigned char Sal, unsigned char *p,unsigned char n)
{
  unsigned char i;
  AMIIC_Start();    // 启动I2C
  AMIIC_Send_Byte(Sal+0x01); //发送器件地址
  /*if(!IIC_Write_Ack())
  {
  	WR_Flag = 1;
		return 0;
  }*/

  delay_us(30); // 延时时间必须大于30us 只要大于 30us 以上的值都可以 但是最好不要太长 ，测试时，试过25MS都OK！ 
        
  for(i=0;i<n-1;i++)  //读取字节数据
  {
     *(p+i)=AMIIC_Read_Byte(1); //读取数据
     //IIC_ACK(); 
  }
  *(p+n-1)=AMIIC_Read_Byte(0);        
  //IIC_NACK();
  AMIIC_Stop(); 
  return 1;	 
}
///计算CRC校验码	
unsigned int CRC16(unsigned char *ptr, unsigned char len)
{
   unsigned int crc=0xffff;
   unsigned char i;
   while(len--)
   {
       crc ^=*ptr++;
       for(i=0;i<8;i++)
	   {
	       if(crc & 0x1)
		   {
		      crc>>=1;
			  crc^=0xa001;
		   }
		   else
		   {
		      crc>>=1;
		   }
	   }
   }
   return crc;
}
///检测CRC校验码是否正确
unsigned char CheckCRC(unsigned char *ptr,unsigned char len)
{
  unsigned int crc;
	crc=(unsigned int)CRC16(ptr,len-2);
	if(ptr[len-1]==(crc>>8) && ptr[len-2]==(crc & 0x00ff))
	{
	    return 0xff;
	}
	else
	{
	   return 0x0;
	}
}
void Waken(void)
{
	AMIIC_Start();       // 启动I2C
	AMIIC_Send_Byte(IIC_Add); // 发送器件地址
	//if(IIC_Write_Ack())
	//{printf("---------");}	       // 唤醒指令时 传感器不会回ACK 但是第一定要发检测ACK的时钟 否则会出错
	
	//delay_ms(2);       // 至少延时1个Ms	说明书里，有个最大值 ，实际当中 你只要大于1MS
	IIC_SCL_L;
	delay_us(800);
	AMIIC_Stop();	
}

void UARTSend_Nbyte(void)
{
	

/***************************************************/
		Clear_Data(); // 清除收到数据
		WR_Flag = 0;
		Waken();	  // 唤醒传感器
		//发送读指令
		WriteNByte(IIC_Add,IIC_TX_Buffer,3); 
		//发送读取或写数据命令后，至少等待2MS（给探头返回数据作时间准备）
		delay_ms(2);    
		//读返回数据
		ReadNByte(IIC_Add,IIC_RX_Buffer,8);
		delay_ms(2);
		IIC_SDA_H;
		IIC_SCL_H;	//确认释放总线
		//通过串口向上发送传感器数据

/***************************************************/
	
	 if(WR_Flag == 0)
	 {
			 if(CheckCRC(IIC_RX_Buffer,8))
			 {						
					Tmp1 = IIC_RX_Buffer[2]*256+IIC_RX_Buffer[3];	   								
					Tmp2 = IIC_RX_Buffer[4]*256+IIC_RX_Buffer[5];				 
			 }

		}
		  			  
} 

void Clear_Data (void)
{
	int i;
	for(i=0;i<IIC_RX_Length;i++)
	{
		IIC_RX_Buffer[i] = 0x00;
	}//接收数据清零
}






