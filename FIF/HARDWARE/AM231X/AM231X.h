#ifndef __AM231X_H
#define __AM231X_H
#include "sys.h"
#include "iic.h"

//内部数据定义
#define IIC_Add 0xB8    //器件地址
#define IIC_RX_Length 15



////字符串定义
//#define S_Function  "Function: 03 04"
//#define S_Temp "Temp:"
//#define S_RH   "RH:"
//#define S_CRCT "CRC: True"
//#define S_CRCF "CRC: Wrong"
//#define S_Data "Data: "
//#define S_NotS "Sensor Not Connected"

u8 WriteNByte(unsigned char sla,unsigned char *s,unsigned char n);
u8 ReadNByte(unsigned char Sal, unsigned char *p,unsigned char n);
unsigned int CRC16(unsigned char *ptr, unsigned char len);
unsigned char CheckCRC(unsigned char *ptr,unsigned char len);
void Waken(void);
void UARTSend_Nbyte(void);
void Clear_Data (void);

#endif

