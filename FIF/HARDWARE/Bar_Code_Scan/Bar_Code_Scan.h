#ifndef __BAR_CODE_SCAN_H
#define __BAR_CODE_SCAN_H
#include "sys.h"
#include "stdio.h"	
#include "string.h" 


//通道选择
#define Add_A       PAout(1)	//
#define Add_B       PAout(4)	//
#define Add_C       PBout(9)	//

//命令类型
#define CMD_READ     		0x07  //读寄存器命令
#define CMD_WRITE       0x08  //写寄存器命令
#define CMD_SAVE        0x09  //存储至EEPROM命令

//扫码模块寄存器地址

#define ADD_WORK_MODE_SET     			0x00  //工作模式配置地址
#define ADD_SLEEP_CONTROL    			  0x07  //休眠控制地址
#define ADD_SERIAL_OUTPUT     			0x0D  //串口输出控制地址
#define ADD_FULL_RANGE_CONTROL      0x2C  //全幅控制地址
#define ADD_CODE128_CONTROL         0x33  //code128控制地址
#define ADD_QR_CONTROL              0x3F  //QR识读控制地址
#define ADD_RANGE_SET          			0xD4  //幅度值设置地址
#define ADD_SETTINGS_SLEEP     			0xD9  //出厂设置与休眠配置地址

//扫码模块控制字
#define CMD_SERIAL_OUTPUT          	0xA0  //串口输出命令  bit1~bit0 = 00：串口输出  01：USB PC键盘
#define CMD_WORK_MODE_SET     			0xD4  //工作模式配置命令
#define CMD_SLEEP_CONTROL     			0x01  //休眠控制命令//bit7=1:启动休眠   bit7 = 0：关闭休眠
#define CMD_FULL_RANGE_CONTROL      0xxx  //全幅使能控制
#define CMD_CODE128_CONTROL         0x05  //code128控制 //bit0 = 1:允许识读Code128码
#define CMD_QR_CONTROL              0x01  //QR识读控制//bit0 = 1:允许识读QR码
#define CMD_RANGE_VALUE    					0x32  //幅度值   0x32 = 50   (0~100%)

#define CMD_FACTORY_SETTINGS     		0x50  //恢复出厂设置命令
#define CMD_USER_DEFINED_SETTINGS   0x55  //恢复用户自定义出厂设置
#define CMD_CS_TO_US          			0x56  //当前设置配置为用户自定义出厂设置 CURRENT SETTINGS TO USER-DEFINED SETTINGS

////主机命令帧
///*---------------------帧----头-类型--长度-地-----址-数据--CRC免校验-------------------------*/
//u8 HOST_Command[]  = {0x7E,0x00,0x07,0x01,0x00,0x2C,0x01,0xAB,0xCD};

////从机响应帧
///*----------------------帧----头-类型--长度-数据--CRC校验-------------------------*/
//u8 SLAVE_Respond[]  = {0x02,0x00,0x00,0x01,0x3E,0xE4,0xAC};


void Bar_Code_Scan_Init(void);
void GPIO_Tunnel_Select(void);
void Open_One_Tunnel(u8 tunnel);
void Close_One_Tunnel(void);
u8 Read_Bar_Code(u8 tunnel,u8 times);

#endif
