#include "Bar_Code_Scan.h"
#include "uart4.h"	 
#include "delay.h"



extern u8  UART4_RX_BUF[]; //接收缓冲,最大UART4_REC_LEN个字节.末字节为换行符 
extern vu8 Received_OK_UHF;
extern vu8 UART4_RX_Counter;
extern u8 Sum_Check(u8 arry[],u16 n);

vu8 Bar_Code_Temp[16] = {0};

 void GPIO_Tunnel_Select(void)
{
	GPIO_InitTypeDef  GPIO_InitStructure;

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOB, ENABLE);	 //使能端口时钟

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_1 | GPIO_Pin_4;	    		 // 
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP; 		 //
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;		 //
	GPIO_Init(GPIOA, &GPIO_InitStructure);	  				     //

	GPIO_SetBits(GPIOA,GPIO_Pin_1 | GPIO_Pin_4); 	

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;	    		 // 
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP; 		 //
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;		 //
	GPIO_Init(GPIOB, &GPIO_InitStructure);	  				     //

	GPIO_SetBits(GPIOB,GPIO_Pin_9); 	
}

/*---------------------------------------------------------------------------------*/
//@ function:条码扫描模块初始化
//@ 1.串口输出；
//@ 2.手动触发；
//@ 3.自定义配置存储；
/*---------------------------------------------------------------------------------*/
void Bar_Code_Scan_Init()
{
	//1.1~6通道依次配置，7通道特殊配置；（7通道为柜门扫描）
	//---1.开启某一通道；
	//---2.发送配置；
	//---3.判断配置成功与否；
	//---4.刷新故障标志；
	//---5.关闭某一通道；
	//2.上报扫码模块状态；
}
	

/*---------------------------------------------------------------------------------*/
//@ function:开启一个通道
//@
/*---------------------------------------------------------------------------------*/
void Open_One_Tunnel(u8 tunnel) {
  tunnel--;
	if(tunnel & 0x01) {
		Add_A = 1;
	} else {
		Add_A = 0;
	}
	if(tunnel & 0x02) {
		Add_B = 1;
	} else {
		Add_B = 0;
	}
	if(tunnel & 0x04) {
		Add_C = 1;
	} else {
		Add_C = 0;
	}
}

/*---------------------------------------------------------------------------------*/
//@ function:关闭一个通道
//@
/*---------------------------------------------------------------------------------*/
void Close_One_Tunnel(void)
{
	Add_A = 1;
	Add_B = 1;
	Add_C = 1;
}

u8 Read_Bar_Code(u8 tunnel,u8 times)
{
	u8 i;
	u8 legal_flag = 0;
	
	UART4_RX_Counter = 0;
	memset( UART4_RX_BUF,'\0',sizeof(UART4_RX_BUF));//memset（）清空数组，效率最高
	
	Open_One_Tunnel(tunnel);//开启指定通道
	while(times--) {
		delay_ms(200);
		if(Received_OK_UHF)//如果扫码模块回复，跳出等待
		{
			break;
		}
	}	
	Close_One_Tunnel();//关闭通道
	
	for(i = 0; i < 16; i++) {
		Bar_Code_Temp[i] = 0x00;
	}
	
	if(Received_OK_UHF)//如果扫码模块回复
	{
		Received_OK_UHF = 0;
		for(i = 0; i < UART4_RX_Counter; i++)
		{			
			if(0x2D == UART4_RX_BUF[i]) //检索“-”
			{				
				legal_flag = 1;//接收到有效信息；	
				break;			
			}
		}
		
		if(legal_flag) { //接收到有效数据
			for(i = 0; i < UART4_RX_Counter-1; i++) { //刷新标签
				Bar_Code_Temp[i] = UART4_RX_BUF[i];
			}
		} else {
			for(i = 0; i < 16; i++) {
				Bar_Code_Temp[i] = 0xFF;
			}
		}
	}
	
	return legal_flag;
}
