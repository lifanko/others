#ifndef __LED_H
#define __LED_H	 
#include "sys.h"

#define SYS_RUN    PCout(1)	  // PC1
#define FAN1       PCout(13)	// PC13
#define FAN2       PCout(14)	// PC14
#define Relay      PBout(5)
#define Buzzer     PBout(6)
#define LIGHT      PBout(7)	// PB7
#define Human_Det  GPIO_ReadInputDataBit(GPIOC,GPIO_Pin_2)

void LED_Init(void);      //初始化
void BUZZER_Init(void);   //蜂鸣器初始化
void FAN_Init(void);      //风扇初始化
void Relay_Init(void);
void Human_Detection_Init(void);
void bee(uint16_t keep);

#endif
