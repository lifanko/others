#include "led.h"
#include "delay.h"

void LED_Init(void)//LED灯带需要初始化为PWM输出模式
{
  GPIO_InitTypeDef  GPIO_InitStructure;

  RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB | RCC_APB2Periph_GPIOC, ENABLE);   //使能PB/C端口时钟

  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_1;          		// 端口配置, 推挽输出
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;      //推挽输出
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;   	//IO口速度为50MHz
  GPIO_Init(GPIOC, &GPIO_InitStructure);                //推挽输出 ，IO口速度为50MHz

  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_7;          		// 端口配置, 推挽输出
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;      //推挽输出
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;     //IO口速度为50MHz
  GPIO_Init(GPIOB, &GPIO_InitStructure);                //推挽输出 ，IO口速度为50MHz

  GPIO_ResetBits(GPIOC, GPIO_Pin_1);        						//系统运行指示灯，初始化开
  GPIO_ResetBits(GPIOB, GPIO_Pin_7);         						//LED灯带，初始化关
}

void BUZZER_Init(void)
{
  GPIO_InitTypeDef  GPIO_InitStructure;
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);   //使能PB端口时钟

  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6;           	// 端口配置, 推挽输出
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;      //推挽输出
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;     //IO口速度为50MHz
  GPIO_Init(GPIOB, &GPIO_InitStructure);                //推挽输出 ，IO口速度为50MHz

  GPIO_ResetBits(GPIOB, GPIO_Pin_6);         						//蜂鸣器关
}

void FAN_Init(void)
{
  GPIO_InitTypeDef  GPIO_InitStructure;
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);   	//使能PC端口时钟

  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_13 | GPIO_Pin_14; 	// 端口配置, 推挽输出
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;      		//推挽输出
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;     		//IO口速度为50MHz
  GPIO_Init(GPIOC, &GPIO_InitStructure);                		//推挽输出 ，IO口速度为50MHz

  GPIO_ResetBits(GPIOC, GPIO_Pin_13 | GPIO_Pin_14);         //风扇关
}

void Relay_Init(void)
{
  GPIO_InitTypeDef  GPIO_InitStructure;
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE); //使能PB端口时钟

  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_5;             // 端口配置, 推挽输出
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;      //推挽输出
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;     //IO口速度为50MHz
  GPIO_Init(GPIOB, &GPIO_InitStructure);                //推挽输出 ，IO口速度为50MHz

  GPIO_ResetBits(GPIOB, GPIO_Pin_5);         						//继电器关
}

void Human_Detection_Init(void)
{
  GPIO_InitTypeDef  GPIO_InitStructure;
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);   //使能PC端口时钟

  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2;             // 端口配置, 推挽输出
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;     		//推挽输出
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;     //IO口速度为50MHz
  GPIO_Init(GPIOC, &GPIO_InitStructure);                //推挽输出 ，IO口速度为50MHz
}

void bee(uint16_t keep) {
	Buzzer = 1;
	delay_ms(keep);
	Buzzer = 0;
	delay_ms(keep);
}
