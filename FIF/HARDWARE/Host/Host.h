#ifndef __HOST_H
#define __HOST_H
#include "sys.h"
#include "stdio.h"
#include "string.h"
#include "Motor_Control.h"
#include "usart2.h"
#include "usart1.h"
#include "uart4.h"

#define  ON    0x01
#define  OFF   0x00

//注意：#include "Motor_Control.C"中11970需要一起更改

#define STEPS_OF_ONE_CIRCLE     12000   //转盘转动一整圈的步数。驱动器分频系数配置一致、齿轮对按照图纸设计，为何整圈步数不同？？？？？
#define GRID_NUM_OF_A_FLOOR     70   //每层转盘书格数
#define STEPS_OF_GRID           171  //STEPS_OF_ONE_CIRCLE / GRID_NUM_OF_A_FLOOR  //单格步数
#define ORIGIN_TO_SCAN_GRIDS    13  //原点与扫码位置之间书格//12
#define ORIGIN_TO_SCAN_STEPS    2229  //  扫码位置距离原点步数( 13 / 70 * 12000 = 2228.57 )

//帧标识
#define Frame_Header       0xAB    //帧头
#define Frame_Tail         0xBA    //帧尾

//帧类型
#define Frame_Type_Order       0x00   //命令帧
#define Frame_Type_Response    0x01   //响应帧
#define Frame_Type_Inform      0x02   //通知帧

//帧指令
#define Serial_Identify      0xBD   //串口识别
#define Version_Query        0x11   //驱动板版本查询
#define Temp_Hum_Query       0x12   //温湿度查询
#define Position_Inquiry     0x13   //转盘位置查询//+单次扫码
#define Self_Detection       0x22   //设备自检
#define Book_Create_table    0x33   //图书建表
#define Borrow_Book          0x44   //借书
#define Return_Book          0x55   //还书
#define Auxiliary_Fuction    0x66   //辅助功能
#define Machine_Status       0x77   //机器状态
#define Module_Fault         0xEE   //模块故障上报

//串口波特率
#define BPS_1200      0xB0
#define BPS_2400      0xB1
#define BPS_4800      0xB2
#define BPS_9600      0xB3
#define BPS_38400     0xB4
#define BPS_57600     0xB5
#define BPS_115200    0xB6

//版本信息
#define HW_Version      0x0B //硬件版本V1.1
#define SW_Version      0x0A //软件版本V1.0

//设备自检//0有效
#define CMD_Self_Detection_1F     			 0x0F3E  //第一层转盘自检
#define CMD_Self_Detection_2F      			 0x0F3D  //第二层转盘自检
#define CMD_Self_Detection_3F     			 0x0F3B  //第三层转盘自检
#define CMD_Self_Detection_4F     			 0x0F37  //第四层转盘自检
#define CMD_Self_Detection_5F      			 0x0F2F  //第五层转盘自检
#define CMD_Self_Detection_6F     			 0x0F1F  //第六层转盘自检
#define CMD_Self_Detection_Push   			 0x0E3F  //推书机构自检
#define CMD_Self_Detection_Put    			 0x0D3F  //出书电梯自检
#define CMD_Self_Detection_Return  			 0x0B3F  //还书电梯自检
#define CMD_Self_Detection_Sub           0x073F  //辅助单元自检

//图书建表//1有效
#define CMD_Create_table_1F     			 0xC1  //第一层图书建表
#define CMD_Create_table_2F     			 0xC2  //第二层图书建表
#define CMD_Create_table_3F     			 0xC3  //第三层图书建表
#define CMD_Create_table_4F     			 0xC8  //第四层图书建表
#define CMD_Create_table_5F     			 0xD0  //第五层图书建表
#define CMD_Create_table_6F     			 0xE0  //第六层图书建表

//设备状态
#define CMD_Machine_Powered        0x01   //设备开机
#define CMD_Machine_Shutdown       0x02   //设备关机
#define CMD_Machine_RST            0x03   //设备复位

//故障代码
#define ERC_LOCK1         0x0001    //锁1
#define ERC_LOCK2         0x0002    //锁2
#define ERC_LOCK3         0x0004    //锁3
#define ERC_LOCK4         0x0008    //锁4
#define ERC_LOCK5         0x0010    //锁5
#define ERC_LOCK6         0x0020    //锁6
#define ERC_LIGHT         0x0040    //灯带
#define ERC_FUN1          0x0080    //风扇1
#define ERC_FUN2          0x0100    //风扇2
#define ERC_UV_LAMP       0x0200    //紫外灯
#define ERC_PIR_SENSOR    0x0400    //红外人体探头
#define ERC_LDR           0x0800    //光敏电阻
#define ERC_RF_BRANCH     0x1000    //射频分支器
#define ERC_UHF_RFID      0x2000    //UHF射频识别模块
#define ERC_DMCM          0x4000    //步进电机控制卡



#define  Book_CheckSelf_SPEED    200     //
#define  Book_FRE800_SPEED       800      //
#define  Book_FRE600_SPEED       600      //
#define  Book_FRE400_SPEED       400      //
#define  Book_FRE200_SPEED       200      //
#define  Book_FRE0_SPEED           0      //


extern u8  USART2_RX_BUF[USART2_REC_LEN]; //接收缓冲,最大USART_REC_LEN个字节.末字节为换行符
extern vu8 Received_OK_RS232;
extern void Single_Poll_Handle(u8 arry[],u16 n);
extern u8 UART4_RX_BUF[UART4_REC_LEN];     //接收缓冲,最大USART_REC_LEN个字节.

extern u16 Angle_To_mV(void);


void Motor_Control_String(char *send, char *fun, char *Motor, int val);
void Motor_Stop_String(char *send, char *fun, char *Motor);

u8 Sum_Check(u8 arry[],u16 n);   //和校验
void Looking_for_Position(void);        //查找空位
void Position_Handle(void);             //返回位置与锁状态信息
//void EPC_Read(void);                    //通道切换、EPC读取
void Serial_Handle(void);               //串口响应数据处理
void Version_Handle(void);              //版本查询数据处理
void Temp_Hum_Handle(void);             //返回温湿度信息
void delay_n_Second(u8 count);

int Get_Current_Steps(void);

void Door_TO_Origin(void);//柜门书格回原点
void Left_Right_Scanning(u8 *array);//左右移动扫码，增加识别率

void Self_Inspection_Handle(void);      //设备自检处理
void Create_Table_Start(void);          //开始建表
void BookInfo_Handle(u8 start_layer,u8 stop_layer);             //返回单本图书信息
void Create_Table_Finish(void);         //建表完成

void Borrow_Book_Start(void);           //开始执行借书操作
void Entire_Process_Handle(void);       //出书全流程处理
void Borrow_Book_Finish(void);   //开始执行借书操作

void Return_Book_Handle(void);          //还书处理
void Auxiliary_Control_Handle(void);    //解析辅助设备控制指令，返回辅助设备状态信息
void Machine_Control_Handle(void);      //解析设备控制指令，返回设备状态信息
void Data_Analysis(void);    //数据解析

void Motor_Contol_init(void);//CY-3202控制卡参数初始化
#endif
