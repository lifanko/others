#include "Host.h"
#include "stdlib.h"
#include "lock.h"
#include "led.h"
#include "usart1.h"
#include "uart5.h"
#include "Bar_Code_Scan.h"
#include "main.h"

/*---------------------------------------------------------------------------------*/

vu8 Lock_Status = 0x00;//全局变量，6个锁的状态,初始状态全锁

u8 Temp_EPC[16] = {0};
vu8 Book_Status = 0x10;  //图书位置：书架，架子，出书口

extern vu8 ATUO_Control;

extern vu8 Lock_Action_Flag;//锁动作标志

extern vu8 Bar_Code_Temp[];

extern u8 Channel_Select_CMD[];
extern u8 XOR_Check(u8 *array);

extern u16 Temperature;
extern u16 Relative_Humidity;

extern u8 Time_Temp_Hum;

extern vu8 USART2_RX_Counter;

extern u8  UART4_RX_BUF[]; //接收缓冲,最大UART4_REC_LEN个字节.末字节为换行符
extern vu8 Received_OK_UHF;
extern vu8 UART4_RX_Counter;

extern u16 adcx;
extern vu16 led0pwmval;

char Motor_Absolute_Move_Array[30];      //绝对移动发送帧
char Motor_Increment_Move_Array[30];     //增量移动发送帧
char Motor_Run_Status_Array[30];         //马达运行状态查询
char Motor1_Locate_Inquire_Array[30];    //马达1位置查询
char Motor1_Locate_Set_Array[30];        //马达1位置更改
char Motor1_Slow_Stop_Array[30];         //马达1慢停


char Motor_Speed_Start_Array[] = "@199:Motor:SStart:M1 10\r\n";       //1.起始速度：10
char Motor_Speed_Stop_Array[]  = "@199:Motor:SStop:M1 10\r\n";        //2.终止速度：10
char Motor_Speed_Add_Array[]   = "@199:Motor:SAdd:M1 2\r\n";          //3.加速度：2
char Motor_Speed_Set_Array[]   = "@199:Motor:Speed:M1 200\r\n";       //4.移动速度：200
char Motor_Speed_Home_Array[]  = "@199:Motor:SHome:M1 200\r\n";       //5.回原点速度：200
char Motor_Invert_Set_Array[]  = "@199:Motor:Invert:M1 1\r\n";        //6.反向：1
char Motor_Limit_Set_Array[]   = "@199:Motor:Limit:M1 0\r\n";         //7.极限：原点关，正极限关；  0
char Motor_Active_Send_State_Array[] = "@199:Program:MINT 1\r\n";     //8.主动上报；
char Motor_Parameters_Save_Array[] =   "@199:Motor:Save \r\n";        //9.保存参数


char Motor_Stop_Array[30];     //停止发送帧

extern void USART1_Write(USART_TypeDef* USARTx, uint8_t *Data, uint8_t len);

_Bool Normal   = 0;  //正常
_Bool Abnormal = 1;  //异常

u8 Auxiliary_Status_MSB = 0x00;//辅助设备状态
u8 Auxiliary_Status_LSB = 0x00;//辅助设备状态

//vu8 Global_Machine_Status_Temp = 0x00;     //全局机器状态中间量，初始化为待机状态
vu8 Global_Machine_Status = 0x00;     //全局机器状态，初始化为待机状态
vu16 Global_Module_fault_Code = 0x00; //全局模块故障码
vu8 Fault_Code_Refresh = 0;//故障代码刷新标志

//故障代码通知帧
/*-------------------帧头-类型-命令--长---度---参1--参2-和校验-帧尾-------------------------*/
u8 ERC_Inform[]  = {0xAB, 0x02, 0xEE, 0x00, 0x02, 0x00, 0x00, 0xF2, 0xBA};


//串口识别响应帧
/*----------------------帧头-类型-命令--长---度--参数-和校验-帧尾-------------------------*/
u8 Serial_Respond[]  = {0xAB, 0x01, 0xBD, 0x00, 0x01, 0xB3, 0x72, 0xBA};

//版本查询响应帧
/*-----------------------帧头-类型-命令--长---度---参1-参2-和校验-帧尾-------------------------*/
u8 Version_Respond[]  = {0xAB, 0x01, 0x11, 0x00, 0x02, 0x0B, 0x0A, 0x29, 0xBA};

//温湿度查询响应帧
/*-------------------------帧头-类型-命令--长---度---参1-参2-和校验-帧尾-------------------------*/
u8 Temp_Hum_Respond[]  = {0xAB, 0x01, 0x12, 0x00, 0x02, 0x19, 0x37, 0x65, 0xBA};

//位置查询响应帧
/*-------------------------帧头-类型-命令--长---度---参1--参2-和校验-帧尾-------------------------*/
u8 Position_Respond[]  = {0xAB, 0x01, 0x13, 0x00, 0x02, 0x01, 0x20, 0x37, 0xBA};

//设备自检响应帧
/*-------------------------------帧头-类型-命令-特殊-和校验-帧尾-------------------------*/
u8 Self_Inspection_Respond[]  = {0xAB, 0x01, 0x22, 0xA2, 0xC5, 0xBA};

//设备自检通知帧
/*-------------------------------帧头-类型-命令--长----度--参1--参2--参3--参4-和校验-帧尾------------------*/
u8 Self_Inspection_Inform[]  = {0xAB, 0x01, 0x22, 0x00, 0x04, 0x01, 0x01, 0x0E, 0x10, 0x47, 0xBA};


//图书建表通知帧
/*----------------------------帧头-类型-命令--长---度--参1--参2-----------------*/
u8 Create_Table_Inform[]  = {0xAB, 0x02, 0x33, 0x00, 0x12, 0x11, 0x20, \

                             0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F, 0xF0, 0xBA
                            };

//图书建表完成通知帧
/*----------------------------------帧头-类型-命令--长--度---参1-和校验-帧尾----------*/
u8 Create_Table_Finish_Inform[]  = {0xAB, 0x02, 0x33, 0x00, 0x01, 0xE3, 0x19, 0xBA};


//借书响应帧
/*----------------------------帧头-类型-命令--长--度---参1-和校验-帧尾------------------*/
u8 Borrow_Book_Respond[]  = {0xAB, 0x01, 0x44, 0x00, 0x01, 0xA4, 0xEA, 0xBA};

//借书通知帧
/*----------------------------帧头-类型-命令--长---度--参1--参2-----------------*/
u8 Borrow_Book_Inform[]  = {0xAB, 0x02, 0x44, 0x00, 0x12, 0x11, 0x20, \

                            0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F, 0xF0, 0xBA
                           };

//还书通知帧
/*----------------------------帧头-类型-命令--长---度--参1--参2-----------------*/
u8 Return_Book_Inform[]  = {0xAB, 0x02, 0x55, 0x00, 0x12, 0x11, 0x20, \

                            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x9A, 0xBA
                           };

//借书完成通知帧
/*----------------------------------帧头-类型-命令--长--度---参1-和校验-帧尾----------*/
u8 Borrow_Book_Finish_Inform[]  = {0xAB, 0x02, 0x44, 0x00, 0x01, 0xE4, 0x2B, 0xBA};

////开锁响应帧
///*--------------------------帧头-类型-命令--长---度--参1-和校验-帧尾------------------*/
//u8 Open_Lock_Respond[]  =  {0xAB,0x01,0x55,0x00,0x01,0x11,0x68,0xBA};

////锁状态改变通知帧
///*--------------------------帧头-类型-命令--长---度--参1-和校验-帧尾------------------*/
//u8 Lock_Action_Respond[]  =  {0xAB,0x01,0x55,0x00,0x01,0x11,0x68,0xBA};


//锁状态通知帧 //在外部中断函数中调用，锁的状态发生改变，上传状态通知帧
/*----------------------------帧头-类型-命令--长---度---参1-参2--和校验-帧尾------------------*/
u8 Lock_Status_Respond[]  =  {0xAB, 0x02, 0x44, 0x00, 0x02, 0x00, 0x01, 0x48, 0xBA};

//还书识别响应帧
/*----------------------------帧头-类型-命令--长---度--参1--和校验-帧尾------------------*/
u8 Return_Read_Respond[]  =  {0xAB, 0x01, 0x55, 0x00, 0x01, 0xA5, 0xFC, 0xBA};

//还书旋转至识别位置响应帧
/*-----------------------------帧头-类型-命令--长---度-和校验-帧尾------------------*/
u8 Return_Rotate_Respond[]  =  {0xAB, 0x01, 0x55, 0x00, 0x00, 0x56, 0xBA};

//还书响应帧
/*----------------------------帧头-类型-命令--长---度--参1-和校验-帧尾------------------*/
u8 Return_Book_Respond[]  =  {0xAB, 0x01, 0x55, 0x00, 0x01, 0xE5, 0x3C, 0xBA};

////辅助设备控制响应帧
///*---------------------------------帧头-类型-命令--长---度--参1--参2-和校验-帧尾------------------*/
//u8 Auxiliary_Control_Respond[] =  {0xAB,0x01,0x66,0x00,0x02,0xFF,0xFE,0x66,0xBA};

//辅助设备状态响应帧
/*---------------------------------帧头-类型-命令--长---度--参1--参2-和校验-帧尾------------------*/
u8 Auxiliary_Status_Respond[]  =  {0xAB, 0x01, 0x66, 0x00, 0x02, 0x00, 0x0F, 0x78, 0xBA};

//设备工作状态查询响应帧
/*-------------------------------帧头-类型-命令--长----度---参1-和校验-帧尾------------------*/
u8 Machine_Status_Respond[]  =  {0xAB, 0x01, 0x77, 0x00, 0x01, 0x20, 0x99, 0xBA};

////模块故障通知帧
///*-----------------------------帧头-类型-命令--长---度---参1---参2-和校验-帧尾------------------*/
//u8 Module_Fault_Respond[]  =  {0xAB,0x02,0xEE,0x00,0x02,0x00,0x01,0xF3,0xBA};

//单次轮询命令帧
/*---------------------------帧-----头---帧---长-类型-Timeout--和校验-帧----尾----*/
u8 Single_Poll_Command[]  = {0xA5, 0x5A, 0x00, 0x0A, 0x80, 0x00, 0x64, 0xEE, 0x0D, 0x0A};

//射频功率配置帧
/*--------------------|帧----头|长-----度|类-型|STA|天线号|读 功 率| 写 功 率 |校验|帧----- 尾----*/
u8 Power_Config[]  = {0xA5, 0x5A, 0x00, 0x0E, 0x10, 0x02, 0x00, 0x0B, 0xB8, 0x0B, 0xB8, 0x1C, 0x0D, 0x0A};

//柜门开关通知应帧
/*----------------------------帧头-类型-命令--长---度--参1--参2-和校验-帧尾------------------*/
u8 DOOR_Action_Inform[]  =  {0xAB, 0x02, 0x33, 0x00, 0x02, 0x00, 0xA3, 0xDA, 0xBA};


// Watch dog block flag
extern u8 block;

// Send close if door not open
extern u8 send_close;

extern char console_val[60];

extern uint8_t block_exti;

/*---------------------------------------------------------------------------------*/
//CY-3202控制卡参数初始化
/*---------------------------------------------------------------------------------*/
void Motor_Contol_init() {
  Log("Set Motor Control Card Start");

  SendCmd(Motor_Speed_Start_Array, "0", 200);       //1.起始速度：10
  SendCmd(Motor_Speed_Stop_Array, "0", 200);        //2.终止速度：10
  SendCmd(Motor_Speed_Add_Array, "0", 200);         //3.加速度：2
  SendCmd(Motor_Speed_Set_Array, "0", 200);         //4.移动速度：200
  SendCmd(Motor_Speed_Home_Array, "0", 200);        //5.回原点速度：200
  SendCmd(Motor_Invert_Set_Array, "0", 200);        //6.反向：1
  SendCmd(Motor_Limit_Set_Array, "0", 200);         //7.极限：原点关，正极限关；  0
  SendCmd(Motor_Active_Send_State_Array, "0", 200); //8.主动上报；
  SendCmd(Motor_Parameters_Save_Array, "0", 500);   //9.保存参数

  Log("Set Motor Control Card End");
}

uint8_t read_lock() {
	uint8_t lock = 0x00;
	
	lock |= Lock1_FB;
  lock |= Lock2_FB << 1;
  lock |= Lock3_FB << 2;
  lock |= Lock4_FB << 3;
  lock |= Lock5_FB << 4;
	
	return lock;
}

uint8_t Get_Lock_Status(void) {
	const uint8_t times = 8;
	uint8_t status[times] = {0x00};
	uint8_t k = 0, j = 0;
	uint8_t buffer = 0x00;
	
	while(k < times && j < 12) {
		j++;
		status[k] = read_lock();
		
		if(buffer != status[k]) {
			buffer = status[k];
			k = 0;
		} else {
			k++;
		}
		delay_ms(25);
	}
	
	if(j >= 12) {
		buffer = 0x00;
		
		block_exti = 1;
	}
	
  sprintf(console_val, "Get Lock Status:%x(HEX), Count:%d", buffer, j);
  Log(console_val);
		
	return buffer;
}

void Motor_Control_String(char *send, char *fun, char *Motor, int val) {
  char Value_String[6] = {0};

  sprintf(Value_String, "%d" , val);//数值转字符串

  memset(send, 0, 30);
  strcpy(send, fun);
  strcat(send, Motor);            //电机位号
  strcat(send, Value_String);     //数值字符串
  strcat(send, CRLF);             //回车换行
}

void Motor_Stop_String(char *send, char *fun, char *Motor) {
  memset(send, 0, 30);
  strcpy(send, fun);
  strcat(send, Motor);            //电机位号
  strcat(send, CRLF);             //回车换行
}

u8 Sum_Check(u8 arry[], u16 n) {
  u8 sum = 0;
  u16 i;

  for (i = 1; i < n + 5; i++) {
    sum += arry[i];
  }
  return sum;
}

u8 Xor_Check(u8 arry[], u16 n) {
  u8 xor_answer = 0;
  u8 i;

  xor_answer = arry[2];
  for (i = 3; i <= n - 4; i++) {
    xor_answer ^= arry[i];
  }
  return xor_answer;
}

void delay_n_Second(u8 count) {
  while (count--) {
    delay_ms(1000);
  }
}
/*----------------------------------------------------------------------------BD-----*/
//串口识别响应帧
///*----------------------帧头-类型-命令--长---度--参数-和校验-帧尾-------------------------*/
//u8 Serial_Respond[]  = {0xAB,0x01,0xBD,0x00,0x01,0xB3,0x72,0xBA};
///*------------------------0----1---2----3----4----5-----6----7-------------------*/
/*---------------------------------------------------------------------------------*/
void Serial_Handle(void)    //串口处理
{
  Serial_Respond[5] =  BPS_9600;//波特率设置为9600
  Serial_Respond[6] = Sum_Check(Serial_Respond, (Serial_Respond[3] << 8) + Serial_Respond[4]);
  USART_SendString_User(USART2, Serial_Respond, 8);
  //  memset( USART2_RX_BUF,'\0',sizeof(USART2_RX_BUF));//memset（）清空数组，效率最高
  //
}

/*-----------------------------------------------------------------------------11----*/
//版本查询响应帧
/*-------------------------帧头-类型-命令--长---度---参1-参2-和校验-帧尾-------------------------*/
//u8 Version_Respond[]  = {0xAB,0x01,0x11,0x00,0x02,0x0B,0x0A,0x29,0xBA};
///*------------------------0----1----2----3----4----5----6----7----8--------------*/
/*---------------------------------------------------------------------------------*/
void Version_Handle(void)   //返回版本信息
{
  Version_Respond[5] =  HW_Version;//硬件版本V1.1
  Version_Respond[6] =  SW_Version;//软件版本V1.0
  Version_Respond[7] =  Sum_Check(Version_Respond, (Version_Respond[3] << 8) + Version_Respond[4]);
  USART_SendString_User(USART2, Version_Respond, 9);
}

/*-----------------------------------------------------------------------------11----*/
//温湿度查询响应帧
/*---------------------------帧头-类型-命令--长---度---参1-参2-和校验-帧尾-------------------------*/
//u8 Temp_Hum_Respond[]  = {0xAB,0x01,0x12,0x00,0x02,0x19,0x37,0x65,0xBA};
///*------------------------0----1----2----3----4----5----6----7----8--------------*/
/*---------------------------------------------------------------------------------*/
void Temp_Hum_Handle(void)   //返回温湿度信息
{
  //获取温度
  //获取湿度
  Temp_Hum_Respond[5] =  Temperature;//温度
  Temp_Hum_Respond[6] =  Relative_Humidity;//湿度
  Temp_Hum_Respond[7] =  Sum_Check(Temp_Hum_Respond, (Temp_Hum_Respond[3] << 8) + Temp_Hum_Respond[4]);
  USART_SendString_User(USART2, Temp_Hum_Respond, 9);
}

/*---------------------------------------------------------------------------------*/
// Get motor steps --- Checked by lifankohome
/*---------------------------------------------------------------------------------*/
int Get_Current_Steps(void) {
  short len;
  char* out;

  int steps;
  int steps_buffer;

  float grids;
  uint8_t grid;

  memset(Motor1_Locate_Inquire_Array, 0, 30);
  strcpy(Motor1_Locate_Inquire_Array, Motor1_Locate_Inquire);

  USART1_Clear();
  USART1_Write(USART1, (unsigned char *)Motor1_Locate_Inquire_Array, strlen((const char *)Motor1_Locate_Inquire_Array));
  delay_ms(100);

  len = USART1_GetRcvNum() - 4;
  if (len <= 0) {
    Log("Get Current Steps: FAIL, Waiting WDG to restart...");

    block = 1;

    while (1);
  }
  out = (char *)malloc(len);     //开辟存储空间
  memcpy(out, (const char *) &USART1_RX_BUF[3], len); //从符号开始复制步数字符串+0x0D
  out[len - 1] = '\0';           //补结束符
  steps = atoi( out );            //字符串转整数
  free(out);

  steps_buffer = steps;

  if (steps >= 12000) {
    // Fixed steps would overflow if spin to one direction too much with button
    steps = steps % 12000;

    // Set control card steps positive
    memset(Motor1_Locate_Set_Array, 0, 30);
    strcpy(Motor1_Locate_Set_Array, Motor1_Locate_Set);
    Motor1_Locate_Set_Array[18] = steps / 10000 + '0';
    Motor1_Locate_Set_Array[19] = steps / 1000 % 10 + '0';
    Motor1_Locate_Set_Array[20] = steps / 100 % 10 + '0';
    Motor1_Locate_Set_Array[21] = steps / 10 % 10 + '0';
    Motor1_Locate_Set_Array[22] = steps % 10 + '0';
    Motor1_Locate_Set_Array[23] = '\r';
    Motor1_Locate_Set_Array[24] = '\n';

    USART1_Clear();
    USART1_Write(USART1, (unsigned char *)Motor1_Locate_Set_Array, strlen((const char *)Motor1_Locate_Set_Array));

    Log("Overflow to Safe");
  }

  if (steps < 0) {
    // Fixed steps would overflow if spin to one direction too much with button
    steps = steps % 12000;
    // Make negative to be positive
    steps += 12000;

    // Set control card steps positive
    memset(Motor1_Locate_Set_Array, 0, 30);
    strcpy(Motor1_Locate_Set_Array, Motor1_Locate_Set);
    Motor1_Locate_Set_Array[18] = steps / 10000 + '0';
    Motor1_Locate_Set_Array[19] = steps / 1000 % 10 + '0';
    Motor1_Locate_Set_Array[20] = steps / 100 % 10 + '0';
    Motor1_Locate_Set_Array[21] = steps / 10 % 10 + '0';
    Motor1_Locate_Set_Array[22] = steps % 10 + '0';
    Motor1_Locate_Set_Array[23] = '\r';
    Motor1_Locate_Set_Array[24] = '\n';

    USART1_Clear();
    USART1_Write(USART1, (unsigned char *)Motor1_Locate_Set_Array, strlen((const char *)Motor1_Locate_Set_Array));

    Log("Negative to Positive");
  }

  grids = steps / (12000.0 / 70.0) + 1.0;
  // Judge which grid now is in
  grid = (uint8_t)grids;
  grid = (grids - grid) > 0.9 ? grid + 1 : grid;

  sprintf(console_val, "Absolute Step:%d, At Grid:%.2f, grid:%d", steps_buffer, grids, grid);
  Log(console_val);

  return steps;
}

/*---------------------------------------------------------------------------------*/
//柜门前书格回原点 Checked by lifankohome
/*---------------------------------------------------------------------------------*/
void Door_TO_Origin(void)
{
  u8 i;

  Log("Rotate To Scan");

  Motor_Control_String(Motor_Increment_Move_Array, Motor_Increment_Move, Motor_Turntable, (0 - ORIGIN_TO_SCAN_STEPS));
  SendCmd(Motor_Increment_Move_Array, "0", 100);
  USART1_Clear();
  for (i = 0; i < 200; i++) {
    delay_ms(200);
    if ((NULL != strstr((const char *)USART1_RX_BUF, "M1"))) {
      break;
    }
  }
}

/*---------------------------------------------------------------------------------*/
//左右移动扫码，增加识别率
//
/*---------------------------------------------------------------------------------*/
void Left_Right_Scanning(u8 *array) {
  u8 i, k;

  uint8_t pic_name[30] = "Inner-Scan:0_00000000-0";
  uint8_t t = 0;

  //1.第一次读取条码信息
  Read_Bar_Code(array[5] & 0x0F, 5); //读取对应位置标签
  if (Bar_Code_Temp[0] != 0) {
    for (k = 0; k < 16; k++) {
      array[7 + k] = Bar_Code_Temp[k];
    }

    pic_name[11] = '1';
    for (t = 0; t < 16; t++) {
      pic_name[t + 13] = Bar_Code_Temp[t];
    }
    Log((char *)pic_name);
    delay_ms(20);

    return;   //检测到非空标签，退出检测
  }
  //2.第二次读取条码信息
  delay_n_Second(2);
  Motor_Control_String(Motor_Increment_Move_Array, Motor_Increment_Move, Motor_Turntable, -30); //组成转盘转动命令
  SendCmd(Motor_Increment_Move_Array, "0", 100); //转盘到位//增量移动必须等待到位
  USART1_Clear();
  for (i = 0; i < 200; i++) //到位等待
  {
    delay_ms(300);
    if ((NULL != strstr((const char *)USART1_RX_BUF, "M1"))) //判断是否有预期的结果
    {
      break;
    }
  }

  Read_Bar_Code(array[5] & 0x0F, 5); //读取对应位置标签
  if (Bar_Code_Temp[0] != 0) {
    for (k = 0; k < 16; k++) {
      array[7 + k] = Bar_Code_Temp[k];
    }
    //返回原来的位置
    delay_n_Second(2);
    Motor_Control_String(Motor_Increment_Move_Array, Motor_Increment_Move, Motor_Turntable, 30); //组成转盘转动命令
    SendCmd(Motor_Increment_Move_Array, "0", 100); //转盘到位//增量移动必须等待到位
    USART1_Clear();
    for (i = 0; i < 200; i++) //到位等待
    {
      delay_ms(300);
      if ((NULL != strstr((const char *)USART1_RX_BUF, "M1"))) //判断是否有预期的结果
      {
        break;
      }
    }

    pic_name[11] = '2';
    for (t = 0; t < 16; t++) {
      pic_name[t + 13] = Bar_Code_Temp[t];
    }
    Log((char *)pic_name);
    delay_ms(20);

    return;   //检测到非空标签，退出检测
  }

  //3.第三次读取条码信息
  delay_n_Second(2);
  Motor_Control_String(Motor_Increment_Move_Array, Motor_Increment_Move, Motor_Turntable, 60); //组成转盘转动命令
  SendCmd(Motor_Increment_Move_Array, "0", 100); //转盘到位//增量移动必须等待到位
  USART1_Clear();
  for (i = 0; i < 200; i++) //到位等待
  {
    delay_ms(300);
    if ((NULL != strstr((const char *)USART1_RX_BUF, "M1"))) //判断是否有预期的结果
    {
      break;
    }
  }

  Read_Bar_Code(array[5] & 0x0F, 5); //读取对应位置标签

  for (k = 0; k < 16; k++) {
    array[7 + k] = Bar_Code_Temp[k];
  }

  if (UART4_RX_Counter > 0) {
    pic_name[11] = '3';
    for (t = 0; t < 16; t++) {
      pic_name[t + 13] = Bar_Code_Temp[t];
    }
  } else {
    pic_name[11] = '4';
  }
  Log((char *)pic_name);
  delay_ms(20);

  //返回原来的位置
  delay_n_Second(2);
  Motor_Control_String(Motor_Increment_Move_Array, Motor_Increment_Move, Motor_Turntable, -30); //组成转盘转动命令
  SendCmd(Motor_Increment_Move_Array, "0", 100); //转盘到位//增量移动必须等待到位
  USART1_Clear();
  for (i = 0; i < 100; i++) //到位等待
  {
    delay_ms(200);
    if ((NULL != strstr((const char *)USART1_RX_BUF, "M1"))) //判断是否有预期的结果
    {
      break;
    }
  }
}
/*-----------------------------------------------------------------------------11----*/
//位置查询响应帧
/*-------------------------帧头-类型-命令--长---度---参1--参2-和校验-帧尾-------------------------*/
//u8 Position_Respond[]  = {0xAB,0x01,0x13,0x00,0x01,0x01,0x20,0x37,0xBA};
///*------------------------0----1----2----3----4----5----6----7----8--------------*/
/*---------------------------------------------------------------------------------*/
void Position_Handle(void)   //返回位置与锁状态信息
{
  int steps;
  float grids;
  uint8_t grid;

  if (0x00 == USART2_RX_BUF[4]) //位置与锁状态信息
  {
    Position_Respond[5] = Get_Lock_Status();//获取锁状态

    steps = Get_Current_Steps();
    grids = steps / (12000.0 / 70.0) + 1.0;

    // Judge which grid now is in
    grid = (uint8_t)grids;
    grid = (grids - grid) > 0.9 ? grid + 1 : grid;

    if (grid > 13) {
      Position_Respond[6] = grid - ORIGIN_TO_SCAN_GRIDS;
    } else {
      Position_Respond[6] = grid - ORIGIN_TO_SCAN_GRIDS + GRID_NUM_OF_A_FLOOR;
    }

    delay_ms(200);
    sprintf(console_val, "Report Grid to Screen:%d", grid);
    Log(console_val);

    Position_Respond[7] = Sum_Check(Position_Respond, (Position_Respond[3] << 8) + Position_Respond[4]);
    USART_SendString_User(USART2, Position_Respond, 9);
  }
  else if (0x12 == USART2_RX_BUF[4]) //查询任意一格条码信息
  {
    Looking_for_Position();
    delay_ms(1000);//!!!防止转向卡顿！！！！
    Return_Book_Inform[5] = USART2_RX_BUF[5];//先赋值，再扫码，否则扫码层数错误
    Return_Book_Inform[6] = USART2_RX_BUF[6];

    Left_Right_Scanning(Return_Book_Inform);//左右移动扫码，增加识别率

    Return_Book_Inform[1] = 0x01;
    Return_Book_Inform[2] = 0x13;

    Return_Book_Inform[23] = Sum_Check(Return_Book_Inform, (Return_Book_Inform[3] << 8) + Return_Book_Inform[4]);
    USART_SendString_User(USART2, Return_Book_Inform, 25); //上报图书信息
  }
}

/*----------------------------------------------------------------------------22-----*/
//设备自检通知帧
/*-------------------------帧头-类型---命令--长---度----参1----参2--和校验-帧尾----------------*/
///*------------------------0----1------2----3-----4-----5-----6------7-----8--------------*/
//*------------------------0xAB 0x02  0x22  0x00  0x02  0x00  0x01  0x27  0xBA
/*---------------------------------------------------------------------------------*/
void Self_Inspection_Handle(void)   //设备自检处理
{
  u8 lock_temp, status;
  u8 Parameter_MSB = 0;
  u8 Parameter_LSB = 0;

  Log("Check Device");

  Global_Machine_Status = Status_Machine_Selfcheck;//设备自检中
  if (0x02 == USART2_RX_BUF[4]) //Rotate to selected rack
  {
    Looking_for_Position();
    delay_ms(1000);//!!!防止转向卡顿！！！！
    Return_Book_Inform[5] = USART2_RX_BUF[5];//先赋值，再扫码，否则扫码层数错误
    Return_Book_Inform[6] = USART2_RX_BUF[6];

    Return_Book_Inform[1] = 0x01;
    Return_Book_Inform[2] = 0x22;

    Return_Book_Inform[23] = Sum_Check(Return_Book_Inform, (Return_Book_Inform[3] << 8) + Return_Book_Inform[4]);
    USART_SendString_User(USART2, Return_Book_Inform, 25); //上报图书信息
  } else if (0x04 == USART2_RX_BUF[4]) //独立模块自检
  {
    lock_temp = USART2_RX_BUF[5];
		
    if (lock_temp <= 0x06) {
      lock_perform(lock_temp);
			Lock_Status = Get_Lock_Status();

      //判断锁正常打开
      if (Lock_Status) {
        Global_Module_fault_Code &= ~lock_temp;//对应的锁正常
      } else {
        Global_Module_fault_Code |= lock_temp;//对应的锁故障

				// 2021-03-04 Cancel lock perform twice, because has to return status less than 500 milliseconds
				// lock_perform(lock_temp);
				// Lock_Status = Get_Lock_Status();

        if (!Lock_Status) {
          Log("Error On Open Door While Checking");
					
					// 2021-01-26 Add: Lock always perform well, or many problems occurs
					Lock_Status |= 0x01 << (lock_temp - 1);
					Lock_Action_Flag = 0x56;
        } else {
					Global_Module_fault_Code &= ~lock_temp;//对应的锁正常
				}
      }
			
			sprintf(console_val, "Check: Open Door:%d, Lock Status:%x(HEX)", lock_temp, Lock_Status);
			Log(console_val);

      status = 1;
      Parameter_MSB = 0x00;
      Parameter_LSB = 0x00;
    }
    else if ((lock_temp >= 0x07) && (lock_temp <= 0x0A)) {
      switch (lock_temp) {
        case 0x07:
          if (USART2_RX_BUF[6]) {
            LIGHT = ON;
          } else {
            LIGHT = OFF;
          };
          break;
        case 0x08:
          if (USART2_RX_BUF[6]) {
            FAN1 = ON;
          } else {
            FAN1 = OFF;
          };
          break;
        case 0x09:
          if (USART2_RX_BUF[6]) {
            FAN2 = ON;
          } else {
            FAN2 = OFF;
          };
          break;
        case 0x0A:
          if (USART2_RX_BUF[6]) {
            Relay = ON;
          } else {
            Relay = OFF;
          };
          break;
        default:
          break;
      }
      //1.延时；
      //2.配置刷新间隔及阈值；
      //3.判断电流是否正常；（）
      //4.刷新当前状态；
      status = 1;
      Parameter_MSB = 0x0E;
      Parameter_LSB = 0x10;
    }
    else if (lock_temp == 0x0B) //红外
    {
      //1.配置刷新间隔；
      //2.读取红外输出；
      //3.刷新当前状态；

      if (1)
      {
        status = 1;
      } else {
        status = 0;
      }
      Parameter_MSB = 0x00;
      Parameter_LSB = 0x00;
    }
    else if (lock_temp == 0x0C) //光敏
    {
      //1.配置刷新间隔及阈值；
      //2.读取当前光强；
      //3.刷新当前状态；

      Parameter_MSB = 0x00;
      Parameter_LSB = 0x96;
      if (((Parameter_MSB << 8) + Parameter_LSB) >= 150)
      {
        status = 1;
      } else {
        status = 1;
      }
    } else { }

    Self_Inspection_Inform[5] =  lock_temp;//自检模块
    Self_Inspection_Inform[6] =  status;//模块状态
    Self_Inspection_Inform[7] =  Parameter_MSB;//参数高字节
    Self_Inspection_Inform[8] =  Parameter_LSB;//参数低字节

    Self_Inspection_Inform[9] = Sum_Check(Self_Inspection_Inform, (Self_Inspection_Inform[3] << 8) + Self_Inspection_Inform[4]);
    USART_SendString_User(USART2, Self_Inspection_Inform, 11);
  } else { }

  Global_Machine_Status = Status_Machine_Standby; //状态：退出机器自检，返回设备空闲态

  //  memset( USART2_RX_BUF,'\0',sizeof(USART2_RX_BUF));//memset（）清空数组，效率最高

}


/*------------------------------------------------------------------------------33---*/
//图书建表响应帧
/*-------------------------------帧头-类型-命令-长---度---参1-和校验-帧尾-----*/
//u8 Create_Table_Respond[]  = {0xAB,0x01,0x33,0x00,0x01,0xF0,0x25,0xBA};
///*-----------------------------0-----1----2----3----4----5----6---7----------*/
/*---------------------------------------------------------------------------------*/
void Create_Table_Start(void)   //开始建表
{
  Global_Machine_Status = Status_Machine_Create_table;//图书建表中
  USART2_RX_BUF[1] = Frame_Type_Response;
  USART2_RX_BUF[6] = Sum_Check(USART2_RX_BUF, (USART2_RX_BUF[3] << 8) + USART2_RX_BUF[4]);
  USART_SendString_User(USART2, USART2_RX_BUF, 8);
}

/*-------------------------------------------------------------------------------33--*/
//图书建表通知帧
/*-----------------------------帧头-类型-命令--长---度--参1--参2--------------------------------------------------------------------------------------------*/
//u8 Create_Table_Inform[]  = {0xAB,0x02,0x33,0x00,0x12,0x11,0x20,0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0A,0x0B,0x0C,0x0D,0x0E,0x0F,0xF0,0xBA};
///*----------------------------0----1----2----3----4----5----6----7----8-----9---10----11--12---13---14---15---16---17---18---19---20---21---22---23---24---------------*/
/*---------------------------------------------------------------------------------*/
//功    能：图书信息上报上位机（默认6层全部建表）
//入口参数：起始层、终止层
//返回参数：无
/*---------------------------------------------------------------------------------*/
void BookInfo_Handle(u8 start_layer, u8 stop_layer)  //返回单本图书信息
{
  u8 i, j, k;
  u8 steps_of_grid;

  sprintf(console_val, "Check Books: From%dTo%d", start_layer, stop_layer);
  Log(console_val);

  //1.转盘回原点（微调固定步数）
  Motor_Turntable_Rst();//转盘复位
  delay_n_Second(4);//防止转向卡顿
  for (j = 70; j > 0; j--) //第70格到第1格
  {
    Create_Table_Inform[6] = j; //格数

    steps_of_grid = (j < 61 && j % 2 == 0) ? 172 : 171;

    Motor_Control_String(Motor_Increment_Move_Array, Motor_Increment_Move, Motor_Turntable, -steps_of_grid); //步进一格
    SendCmd(Motor_Increment_Move_Array, "0", 10); //转盘开始转动

    memset(Motor_Run_Status_Array, 0, 30);
    strcpy(Motor_Run_Status_Array, Motor_Run_Status);
    strcat(Motor_Run_Status_Array, CRLF);             //回车换行

    SendCmd(Motor_Run_Status_Array, "R:00000", 100); //等待电机停止
    delay_ms(200);

    for (i = start_layer; i <= stop_layer; i++) {
      Create_Table_Inform[5] = 0x10 | i;   //（bit7~bit4）：图书状态   （bit3~bit0）：层数

      Read_Bar_Code(i, 10); //读取对应位置标签

      for (k = 0; k < 16; k++) {
        Create_Table_Inform[7 + k] = Bar_Code_Temp[k];
      }
      Create_Table_Inform[23] = Sum_Check(Create_Table_Inform, (Create_Table_Inform[3] << 8) + Create_Table_Inform[4]);
      USART_SendString_User(USART2, Create_Table_Inform, 25);
      delay_ms(200);
    }
  }

}
/*------------------------------------------------------------------------------33---*/
//图书建表完成通知帧
/*----------------------帧头--类型--命令--长----度----参1--和校验-帧尾-----------------------*/
//*---------------------0xAB  0x02  0x33  0x00  0x01  0xE3  0x19  0xBA
///*---------------------0-----1-----2-----3-----4-----5------6----7-------*/
/*---------------------------------------------------------------------------------*/
void Create_Table_Finish(void)   //建表完成处理
{
  Global_Machine_Status = Status_Machine_Standby;//图书建表完成，返回设备空闲状态
  USART_SendString_User(USART2, Create_Table_Finish_Inform, 8);
}


/*-------------------------------------------------------------------------------44--*/
//借书开始响应帧
/*----------------------帧头--类型--命令--长----度----参1--和校验-帧尾-------------------------*/
/*----------------------0xAB  0x01  0x44  0x00  0x01  0xA4  0xEA  0xBA
  //------------------------0-----1-----2-----3-----4-----5------6----7------*/
void Borrow_Book_Start(void)   //开始执行借书操作
{
  Global_Machine_Status = Status_Machine_Borrow_Book;//借书中
  USART_SendString_User(USART2, Borrow_Book_Respond, 8);
}

/*-------------------------------------------------------------------------------44--*/
//借书完成通知帧
/*----------------------帧头--类型--命令--长----度----参1--和校验-帧尾-------------------------*/
/*----------------------0xAB  0x02  0x44  0x00  0x01  0xE4  0x2B  0xBA
  //------------------------0-----1-----2-----3-----4-----5------6----7------*/
void Borrow_Book_Finish(void)   //开始执行借书操作
{
  Global_Machine_Status = Status_Machine_Standby; //退出借书状态，返回待机
  USART_SendString_User(USART2, Borrow_Book_Finish_Inform, 8);
}

void goto_grid(uint8_t target_grids) {
  char console_val[90];

  uint8_t i = 0;
  uint16_t current_step = 0;

  int16_t offset = 0;
  int16_t step;

  float step_to_grid = 12000.0 / 70.0 * (target_grids - 1);

  current_step = Get_Current_Steps();

  offset = step_to_grid - current_step;

  // clockwise
  if (offset < -6000 && current_step > 6000) {
    step_to_grid += 12000;
    offset += 12000;
  }

  // !clockwise
  if (offset >= 6000) {
    step_to_grid = -(12000.0 / 70.0 * (70 - (target_grids - 1)));
    offset -= 12000;
  }

  // Judge which step to go
  step = step_to_grid;
  if (step_to_grid > step) {
    step = (step_to_grid - step) > 0.5 ? step + 1 : step;
  } else {
    step = (step_to_grid - step) < -0.5 ? step - 1 : step;
  }

  offset = step - current_step;

  sprintf(console_val, "Current Step:%d, Target Step:%d, Target Grids:%d, At Step:%.2f, Offset:%d", current_step, step, target_grids, step_to_grid, offset);
  Log(console_val);

  if (offset >= -1 && offset <= 1) {
    Buzzer = 1;
    delay_ms(30);
    Buzzer = 0;
  }

  if (offset != 0) {
    Motor_Control_String(Motor_Absolute_Move_Array, Motor_Absolute_Move, Motor_Turntable, step);

    USART1_Clear();

    SendCmd(Motor_Absolute_Move_Array, "0", 100); //转盘开始转动

    for (i = 0; i < 200; i++) //到位等待,超时时间500*200ms
    {
      delay_ms(500);
      if ((NULL != strstr((const char *)USART1_RX_BUF, "M1"))) //判断是否有预期的结果
      {
        break;
      }
    }
  }
}

//------------------------------------------------------------------------------44--*/
//查询空位
/*-----------------------------帧头-类型-命令--长---度--参1--参2--------------------------------------------------------------------------------------------*/
//u8 Borrow_Book_Inform[]  = {0xAB,0x02,0x44,0x00,0x12,0x11,0x20,0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0A,0x0B,0x0C,0x0D,0x0E,0x0F,0x01,0xBA};
///*----------------------------0----1----2----3----4----5----6----7----8-----9---10----11--12---13---14---15---16---17---18---19---20---21---22---23---24---------------*/
/*---------------------------------------------------------------------------------*/
void Looking_for_Position(void)   //查找空位
{
  u16 target_grids;

  target_grids = (USART2_RX_BUF[2] != 0x13) ? (USART2_RX_BUF[6] + 13) % 70 : USART2_RX_BUF[6];

  goto_grid(target_grids);
}


//------------------------------------------------------------------------------44--*/
//借书流程通知帧
/*-----------------------------帧头-类型-命令--长---度--参1--参2--------------------------------------------------------------------------------------------*/
//u8 Borrow_Book_Inform[]  = {0xAB,0x02,0x44,0x00,0x12,0x11,0x20,0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0A,0x0B,0x0C,0x0D,0x0E,0x0F,0x01,0xBA};
///*----------------------------0----1----2----3----4----5----6----7----8-----9---10----11--12---13---14---15---16---17---18---19---20---21---22---23---24---------------*/
/*---------------------------------------------------------------------------------*/
void Entire_Process_Handle(void)   //出书全流程处理
{
  u8 lock_temp, i;

  //关闭原点使能？？？？？？？？？？？？
  if (0x12 == USART2_RX_BUF[4]) //Android板下发目标图书信息
  {
    //开始借书
    Borrow_Book_Start();
    Looking_for_Position();

    //3-1.向主机发送图书信息；
    //PS:仅在图书自检、还书两种工作状态下对标签进行识别，借书无需识别；

    for (i = 0; i < 16; i++)
    {
      Borrow_Book_Inform[7 + i] = USART2_RX_BUF[7 + i];
    }

    Borrow_Book_Inform[5] = Book_Status | USART2_RX_BUF[5];   //（bit7~bit4）：图书状态   （bit3~bit0）：层数
    Borrow_Book_Inform[6] = USART2_RX_BUF[6];          //格数
    Borrow_Book_Inform[23] = Sum_Check(Borrow_Book_Inform, (Borrow_Book_Inform[3] << 8) + Borrow_Book_Inform[4]);
    USART_SendString_User(USART2, Borrow_Book_Inform, 25); //上报图书信息
  }
  else if (0x02 == USART2_RX_BUF[4]) //开锁
  {
    lock_temp = USART2_RX_BUF[6];//第几层锁

    lock_act(lock_temp);
		Lock_Status = Get_Lock_Status();

    //判断锁正常打开
    if (Lock_Status) {
      Global_Module_fault_Code &= ~lock_temp;//对应的锁正常
    } else {
      Global_Module_fault_Code |= lock_temp;//对应的锁故障

			// 2021-03-04 Cancel lock perform twice, because has to return status less than 500 milliseconds
			// lock_act(lock_temp);
			// Lock_Status = Get_Lock_Status();

      if (!Lock_Status) {
        Log("Error On Open Door While Borrowing But Set Success");
				
				// 2021-01-26 Add: Lock always perform well, or many problems occurs
				Lock_Status |= lock_temp;
				Lock_Action_Flag = 0x56;

        send_close = 1;
      }
    }
		
		sprintf(console_val, "Borrowing: Open Door:%d, Lock Status:%x(HEX)", lock_temp, Lock_Status);
    Log(console_val);
  }
}


/*-------------------------------------------------------------------------------55--*/
//还书响应帧
/*------------------------------帧头-类型-命令--长---度--参1-和校验-帧尾------------------*/
//u8 Return_Book_Respond[]  =  {0xAB,0x01,0x55,0x00,0x01,0xE5,0x3C,0xBA};
///*-----------------------------0----1----2----3----4----5----6----7------------------*/
/*---------------------------------------------------------------------------------*/
void Return_Book_Handle(void)   //返回还书信息
{
  vu8  temp = 0;//识别指令保存
  u8 lock_temp;
  u8 i;
  static u8 return_flag = 0;
  Global_Machine_Status = Status_Machine_Return_Book;//还书中

  if (0x01 == USART2_RX_BUF[4]) //Android板下发开始识别命令
  {
    //0.响应帧,表示打开扫码模块，开始识别
    if (0xA5 == USART2_RX_BUF[5]) {
      return_flag = 0;

      Return_Read_Respond[5] =  0xA5;
      Return_Read_Respond[6] = Sum_Check(Return_Read_Respond, (Return_Read_Respond[3] << 8) + Return_Read_Respond[4]);
      USART_SendString_User(USART2, Return_Read_Respond, 8);
    }
    //2.2.结束
    if (0xE5 == USART2_RX_BUF[5]) {
      return_flag = 6;
      Return_Read_Respond[5] =  0xE5; //识别结束
      Return_Read_Respond[6] = Sum_Check(Return_Read_Respond, (Return_Read_Respond[3] << 8) + Return_Read_Respond[4]);
      USART_SendString_User(USART2, Return_Read_Respond, 8);
    }
    //1.判断开始/结束
    //2.1.开始
    if (return_flag <= 5) {
      return_flag++;
      i = Read_Bar_Code(7, 10); //开启柜门扫码器，获取标签数据
			
			if(i) {
				tip(100);
			}

      for (i = 0; i < 16; i++) {
        Return_Book_Inform[7 + i] = Bar_Code_Temp[i];
      }

      Return_Book_Inform[5]  = 0;//识别状态，层格为0
      Return_Book_Inform[6]  = 0;//识别状态，层格为0

      Return_Book_Inform[1] = 0x02;//通知帧
      Return_Book_Inform[2] = 0x55;//
      Return_Book_Inform[23] = Sum_Check(Return_Book_Inform, (Return_Book_Inform[3] << 8) + Return_Book_Inform[4]);
      USART_SendString_User(USART2, Return_Book_Inform, 25); //上报空格位置图书信息

      //清除EPC，保证同一本书下次进入仍然上传
      for (i = 0; i < 16; i++) {
        Return_Book_Inform[7 + i] = 0;
      }
    }
  }
  else if (0x12 == USART2_RX_BUF[4]) //Android板下发空位信息
  {
    Looking_for_Position();//查询空位

    for (i = 0; i < 16; i++) {
      Return_Book_Inform[7 + i] = Bar_Code_Temp[i];
    }
    Return_Book_Inform[1] = 0x01;//响应
    Return_Book_Inform[2] = 0x55;//
    Return_Book_Inform[5] = USART2_RX_BUF[5];
    Return_Book_Inform[6] = USART2_RX_BUF[6];
    Return_Book_Inform[23] = Sum_Check(Return_Book_Inform, (Return_Book_Inform[3] << 8) + Return_Book_Inform[4]);
    USART_SendString_User(USART2, Return_Book_Inform, 25); //上报空格位置图书信息
  }
  else if (0x02 == USART2_RX_BUF[4]) //开锁
  {
    lock_temp = USART2_RX_BUF[6];//第几层锁

    lock_act(lock_temp);
		Lock_Status = Get_Lock_Status();

    //判断锁正常打开
    if (Lock_Status) {
      Global_Module_fault_Code &= ~lock_temp;//对应的锁正常
    } else {
      Global_Module_fault_Code |= lock_temp;//对应的锁故障

			// 2021-03-04 Cancel lock perform twice, because has to return status less than 500 milliseconds
			// lock_act(lock_temp);
			// Lock_Status = Get_Lock_Status();

      if (!Lock_Status) {
        Log("Error On Open Door While Returning");
      } else {
				Global_Module_fault_Code &= ~lock_temp;//对应的锁正常
			}
    }
		
		if (Lock_Status) {
			sprintf(console_val, "Returning: Open Door:%d, Lock Status:%x(HEX)", lock_temp, Lock_Status);
			Log(console_val);
		}
  } else if (0x00 == USART2_RX_BUF[4]) //旋转至扫码位
  {
    //0.响应帧,表示驱动板控制转盘开始转动
    Return_Rotate_Respond[5] = Sum_Check(Return_Rotate_Respond, (Return_Rotate_Respond[3] << 8) + Return_Rotate_Respond[4]);
    USART_SendString_User(USART2, Return_Rotate_Respond, 7);

    Door_TO_Origin();//柜门书格回原点 //转盘回到天线位置；

    Return_Book_Inform[2] = USART2_RX_BUF[5];//
    Left_Right_Scanning(Return_Book_Inform);//左右移动扫码，增加识别率

    Return_Book_Inform[1] = 0x02;//通知帧
    Return_Book_Inform[2] = 0x55;//还书指令
    Return_Book_Inform[23] = Sum_Check(Return_Book_Inform, (Return_Book_Inform[3] << 8) + Return_Book_Inform[4]);
    USART_SendString_User(USART2, Return_Book_Inform, 25); //上报空格位置图书信息

    //全局变量，设备状态改变
    Global_Machine_Status = Status_Machine_Standby;//还书完成，返回待机
  }
}

/*-------------------------------------------------------------------------------33--*/
//消毒驱动转盘旋转
/*---------------------------------------------------------------------------------*/
//功    能：消毒旋转
//入口参数：
//返回参数：
/*---------------------------------------------------------------------------------*/
void Disinfect_Revolve(uint8_t disinfect_time)   //
{
  u8 j;
  u8 steps_of_grid;
  uint8_t disinfect_buffer = disinfect_time;

  Log("Disinfect Start");

  for (j = 70; j > 0; j--) //第70格到第1格
  {
    steps_of_grid = (j < 61 && j % 2 == 0) ? 172 : 171;

    Motor_Control_String(Motor_Increment_Move_Array, Motor_Increment_Move, Motor_Turntable, -steps_of_grid); //步进一格
    SendCmd(Motor_Increment_Move_Array, "0", 10); //转盘开始转动

    memset(Motor_Run_Status_Array, 0, 30);
    strcpy(Motor_Run_Status_Array, Motor_Run_Status);
    strcat(Motor_Run_Status_Array, CRLF);             //回车换行

    SendCmd(Motor_Run_Status_Array, "R:00000", 100); //等待电机停止

    disinfect_time = disinfect_buffer;

    //消毒30秒（可配置）
    while (disinfect_time--)
    {
      delay_ms(1000);

      Buzzer = 1;
      delay_ms(50);
      Buzzer = 0;
    }
  }

  Log("Disinfect End");

  Motor_Turntable_Rst();//转盘复位
  delay_n_Second(3);//防止转向卡顿

  Auxiliary_Status_Respond[1] =  0x02;

  USART2_RX_BUF[6] = USART2_RX_BUF[6] & ~(1 << 1);//更新模块状态

  Auxiliary_Status_LSB =  USART2_RX_BUF[6];//更新模块状态

  Auxiliary_Status_Respond[5] =  Auxiliary_Status_MSB;//备用
  Auxiliary_Status_Respond[6] =  Auxiliary_Status_LSB;//bit4：风扇2； bit3：风扇1； bit2：紫外消毒； bit0:照明1

  Auxiliary_Status_Respond[7] = Sum_Check(Auxiliary_Status_Respond, (Auxiliary_Status_Respond[3] << 8) + Auxiliary_Status_Respond[4]);
  USART_SendString_User(USART2, Auxiliary_Status_Respond, 9);

}

/*-------------------------------------------------------------------------------66--*/
//辅助功能控制响应帧
/*-------------------------帧头--类型-命令--长---度---参1---参2--和校验-帧尾-------------------------*/
/*-------------------------0xAB 0x01  0x66 0x00 0x02  0x00  0x01  0x69  0xBA
  ///-------------------------0----1-----2----3----4-----5-----6-----7-----8----------------*/
/*---------------------------------------------------------------------------------*/
void Auxiliary_Control_Handle(void)   //解析辅助设备控制指令，返回辅助设备状态信息
{
  static uint8_t disinfect_flag = 0;

  if (USART2_RX_BUF[4]) //下发控制命令
  {
    //通过各种检测手段判断各模块工作是否正常。例如：增加电流检测，判断风扇是否正常
    //    Global_Module_fault_Code &= ~lock_temp;//对应的锁正常<<<<<<<<<<<<<<<<<<<<<<
    if (USART2_RX_BUF[6] & 0x01)
    {
      //            LIGHT = ON;
      //打开照明1

      led0pwmval = 50;//亮度调暗
      TIM_SetCompare2(TIM4, led0pwmval);
    } else {
      //            LIGHT = OFF;
      //关闭照明1

      led0pwmval = 170;//亮度调暗
      TIM_SetCompare2(TIM4, led0pwmval);
    }

    if (USART2_RX_BUF[6] & 0x02) {
      Relay = ON;
      Disinfect_Revolve(USART2_RX_BUF[5] <= 5 ? 5 : USART2_RX_BUF[5]);
      Relay = OFF;

      disinfect_flag = 1;
    } else {
      //关闭紫外
      Relay = OFF;
    }

    if (USART2_RX_BUF[6] & 0x04) {
      FAN1 = ON;
      //打开风扇1
    } else {
      FAN1 = OFF;
      //关闭风扇1
    }
    if (USART2_RX_BUF[6] & 0x08) {
      FAN2 = ON;
      //打开风扇2
    } else {
      FAN2 = OFF;
      //关闭风扇2
    }
  }
  if (!disinfect_flag) {
    Auxiliary_Status_Respond[1] =  0x01;

    Auxiliary_Status_LSB = 0x00 | (FAN2 << 3) | (FAN1 << 2) | (FAN2 << 1) | Relay;//更新模块状态

    Auxiliary_Status_Respond[5] =  Auxiliary_Status_MSB;//备用
    Auxiliary_Status_Respond[6] =  Auxiliary_Status_LSB;//bit4：风扇2； bit3：风扇1； bit2：紫外消毒； bit0:照明1

    Auxiliary_Status_Respond[7] = Sum_Check(Auxiliary_Status_Respond, (Auxiliary_Status_Respond[3] << 8) + Auxiliary_Status_Respond[4]);
    USART_SendString_User(USART2, Auxiliary_Status_Respond, 9);
  } else {
    disinfect_flag = 0;
  }
}

/*-------------------------------------------------------------------------------77--*/
//设备工作状态控制及检测
/*-------------------------帧头--类型-命令--长-----度----参1-和校验-帧尾-------------------------*/
/*-------------------------0xAB 0x01  0x77  0x00  0x01  0x20  0x99  0xBA
  ///-------------------------0----1-----2-----3-----4-----5-----6-----7-----------------*/
/*---------------------------------------------------------------------------------*/
void Machine_Control_Handle(void)   //解析辅助设备控制指令，返回辅助设备状态信息
{
  if (USART2_RX_BUF[4]) //下发控制命令
  {
    if (CMD_Machine_Powered == USART2_RX_BUF[5]) {
      Global_Machine_Status = Status_Machine_On;
      //开机
    } else if (CMD_Machine_Shutdown == USART2_RX_BUF[5]) {
      Global_Machine_Status = Status_Machine_Off;
      //关机
    } else if (CMD_Machine_RST == USART2_RX_BUF[5]) {
      Global_Machine_Status = Status_Machine_Standby;
      //复位，返回待机
    }
  }

  Machine_Status_Respond[5] = Global_Machine_Status;//设备运行状态
  Machine_Status_Respond[6] = Sum_Check(Machine_Status_Respond, (Machine_Status_Respond[3] << 8) + Machine_Status_Respond[4]);
  USART_SendString_User(USART2, Machine_Status_Respond, 8);
}

/*---------------------------------------------------------------------------------*/
//对上位机发送的指令帧进行解析
//
/*---------------------------------------------------------------------------------*/
void Data_Analysis(void)    //数据解析
{
  u16 Sumcheck_length = 0;
  u8 Sum_Check = 0;
  u8 i;
  u8 start_floor = 1, end_floor = 5;

  if (Received_OK_RS232) {   //判断串口2空闲中断是否触发
    Received_OK_RS232 = 0;

    if (Frame_Type_Order == USART2_RX_BUF[1])  //驱动板接收到命令帧
    {
      //命令码合法
      if ((Serial_Identify == USART2_RX_BUF[2]) || (Version_Query == USART2_RX_BUF[2]) || (Temp_Hum_Query == USART2_RX_BUF[2])\
          || (Position_Inquiry == USART2_RX_BUF[2]) || (Self_Detection == USART2_RX_BUF[2]) || (Book_Create_table == USART2_RX_BUF[2])\
          || (Borrow_Book == USART2_RX_BUF[2]) || (Return_Book == USART2_RX_BUF[2])\
          || (Auxiliary_Fuction == USART2_RX_BUF[2]) || (Machine_Status == USART2_RX_BUF[2]))
      {
        Sumcheck_length = (USART2_RX_BUF[3] << 8) + USART2_RX_BUF[4] + 5;
        for (i = 1; i < Sumcheck_length; i++) {
          Sum_Check +=  USART2_RX_BUF[i];
        }
        if (USART2_RX_BUF[Sumcheck_length] == Sum_Check) //和校验正确
        {
          if ((Self_Detection == USART2_RX_BUF[2]) || (Auxiliary_Fuction == USART2_RX_BUF[2])) //半功能
          {
            ATUO_Control = 0;
          } else {
            ATUO_Control = 1;
          }

          sprintf(console_val, "Received Cmd: 0:%X 1:%X 2:%X 3:%X 4:%X 5:%X 6:%X", USART2_RX_BUF[0], USART2_RX_BUF[1], USART2_RX_BUF[2], USART2_RX_BUF[3], USART2_RX_BUF[4], USART2_RX_BUF[5], USART2_RX_BUF[6]);
          Log(console_val);

          switch (USART2_RX_BUF[2])
          {
            case Serial_Identify:  //串口识别命令
              Serial_Handle();
              break;
            case Version_Query:  //版本信息查询命令
              Version_Handle();
              break;
            case Temp_Hum_Query:  //温湿度查询命令
              if (0x00 == USART2_RX_BUF[4]) //查询
              {
                Temp_Hum_Handle();
              } else {//配置自动上传间隔
                Time_Temp_Hum = USART2_RX_BUF[7];
                //此处应添加回复？？？？？？？？？？？？？？？？？
              }
              break;
            case Position_Inquiry:  //转盘位置与锁状态查询
              Position_Handle();
              break;
            case Self_Detection:  //机器自检命令
              Self_Inspection_Handle();
              break;
            case Book_Create_table:     //图书建表命令
              Create_Table_Start();  //开始建表
              switch (USART2_RX_BUF[5]) //单层或全部自检
              {
                case 0x01:
                  start_floor = 1;
                  end_floor = 1;
                  break;
                case 0x02:
                  start_floor = 2;
                  end_floor = 2;
                  break;
                case 0x04:
                  start_floor = 3;
                  end_floor = 3;
                  break;
                case 0x08:
                  start_floor = 4;
                  end_floor = 4;
                  break;
                case 0x10:
                  start_floor = 5;
                  end_floor = 5;
                  break;
                default:
                  break;
              }
              BookInfo_Handle(start_floor, end_floor);    //上传所有图书信息
              Create_Table_Finish(); //完成建表
              break;
            case Borrow_Book:  //借书命令
              Entire_Process_Handle();//开始执行借书操作
              break;
            case Return_Book:  //还书命令
              Return_Book_Handle();
              break;
            case Auxiliary_Fuction:  //辅助单元控制命令
              Auxiliary_Control_Handle();
              break;
            case Machine_Status:  //机器状态控制及查询命令
              Machine_Control_Handle();
              break;
            default:
              break;
          }
        } else {}//和校验错误
      } else {}//命令码非法
    } else {}//驱动板未接收到命令帧

    USART2_RX_Counter = 0;
  }
}
