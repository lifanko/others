#ifndef __LOCK_H
#define __LOCK_H	 
#include "sys.h"

#define Lock_EN PAout(12)

#define Lock1 PCout(4)	// PC4
#define Lock2 PBout(0)	// PB0
#define Lock3 PBout(12)	// PB12
#define Lock4 PBout(14)	// PB14
#define Lock5 PAout(8)	// PA8

#define Lock1_FB  GPIO_ReadInputDataBit(GPIOC,GPIO_Pin_5)
#define Lock2_FB  GPIO_ReadInputDataBit(GPIOB,GPIO_Pin_1)
#define Lock3_FB  GPIO_ReadInputDataBit(GPIOB,GPIO_Pin_13)
#define Lock4_FB  GPIO_ReadInputDataBit(GPIOB,GPIO_Pin_15)
#define Lock5_FB  GPIO_ReadInputDataBit(GPIOC,GPIO_Pin_9)

void LOCK_Init(void);
void LOCK_FB_Init(void);
void lock_act(uint8_t lock_temp);
void lock_perform(uint8_t lock);

#endif
