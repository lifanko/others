#include "lock.h"
#include "led.h"
#include "delay.h"

#define lock_delay 150

extern uint8_t uptime;

void LOCK_Init(void)
{
	GPIO_InitTypeDef  GPIO_InitStructure;

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOB | RCC_APB2Periph_GPIOC, ENABLE);	 //使能端口时钟

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8 | GPIO_Pin_12;									// 端口配置, 推挽输出
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;												// 推挽输出
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;												// IO口速度为50MHz
	GPIO_Init(GPIOA, &GPIO_InitStructure);																	// 推挽输出 ，IO口速度为50MHz

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_12 | GPIO_Pin_14;		// 端口配置, 推挽输出
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;												// 推挽输出
	GPIO_Init(GPIOB, &GPIO_InitStructure);																	// 推挽输出 ，IO口速度为50MHz

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_4;																// 端口配置, 推挽输出
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;												// 推挽输出
	GPIO_Init(GPIOC, &GPIO_InitStructure);																	// 推挽输出 ，IO口速度为50MHz

	GPIO_ResetBits(GPIOA,GPIO_Pin_8 | GPIO_Pin_12); 				                // 输出
	GPIO_ResetBits(GPIOB,GPIO_Pin_0 | GPIO_Pin_12 | GPIO_Pin_14); 				  // 输出
	GPIO_ResetBits(GPIOC,GPIO_Pin_4); 				                            	// 输出
}

void LOCK_FB_Init(void) {
 GPIO_InitTypeDef  GPIO_InitStructure;
 	
 RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOB | RCC_APB2Periph_GPIOC, ENABLE);	 //使能端口时钟
 
 GPIO_InitStructure.GPIO_Pin = GPIO_Pin_1 | GPIO_Pin_13 | GPIO_Pin_15;
 GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
 GPIO_Init(GPIOB, &GPIO_InitStructure);
	
 GPIO_InitStructure.GPIO_Pin = GPIO_Pin_5 | GPIO_Pin_9;
 GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
 GPIO_Init(GPIOC, &GPIO_InitStructure);
}

void lock_act(uint8_t lock_temp) {
	Buzzer = 1;
	
	Lock_EN = 1;
  switch (lock_temp)
  {
    case 0x01:
      Lock1 = 1;
      delay_ms(lock_delay);
      Lock1 = 0;
      break;
    case 0x02:
      Lock2 = 1;
      delay_ms(lock_delay);
      Lock2 = 0;
      break;
    case 0x04:
      Lock3 = 1;
      delay_ms(lock_delay);
      Lock3 = 0;
      break;
    case 0x08:
      Lock4 = 1;
      delay_ms(lock_delay);
      Lock4 = 0;
      break;
    case 0x10:
      Lock5 = 1;
      delay_ms(lock_delay);
      Lock5 = 0;
      break;
    default:
      break;
  }
	Lock_EN = 0;
	
	Buzzer = 0;
	
	// Report uptime
	uptime = 1;
}

void lock_perform(uint8_t lock) {
  lock_act(0x01 << (lock - 1));
}
