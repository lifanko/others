#ifndef __IIC_H
#define __IIC_H
#include "sys.h"
#include "delay.h"

#define AMIIC_SCL 	GPIO_Pin_4//PB4
#define AMIIC_SDA 	GPIO_Pin_3//PB3
#define GPIO_IIC 	GPIOB

#define IIC_SCL_H GPIO_SetBits(GPIO_IIC,AMIIC_SCL)
#define IIC_SCL_L GPIO_ResetBits(GPIO_IIC,AMIIC_SCL)
#define IIC_SDA_H GPIO_SetBits(GPIO_IIC,AMIIC_SDA)
#define IIC_SDA_L GPIO_ResetBits(GPIO_IIC,AMIIC_SDA)

void IIC_INIT(void);
void IIC_SDA_OUT(void);
void IIC_SDA_IN(void);
void AMIIC_Start(void);
void AMIIC_Stop(void);
void AMIIC_ACK(void);
void AMIIC_NACK(void);
u8 AMIIC_Write_Ack(void);
void AMIIC_Send_Byte(u8 txd);
u8 AMIIC_Read_Byte(u8 ack);

#endif

