#include "iic.h"

void IIC_INIT(void)
{
	GPIO_InitTypeDef  GPIO_InitStructure;
 	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB|RCC_APB2Periph_AFIO, ENABLE);	 //使能PB端口时钟
	GPIO_PinRemapConfig(GPIO_Remap_SWJ_JTAGDisable, ENABLE);
	GPIO_InitStructure.GPIO_Pin = AMIIC_SCL|AMIIC_SDA;				 
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP; 		 //推挽输出
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;		 //IO口速度为50MHz
	GPIO_Init(GPIOB, &GPIO_InitStructure);					 //根据设定参数初始化GPIOB.5
	
	IIC_SCL_H;		IIC_SDA_H;		//将SCL和SDA都拉高
	
}
void IIC_SDA_OUT(void)//SDA输出
{
	GPIO_InitTypeDef  GPIO_InitStructure;
	
	GPIO_InitStructure.GPIO_Pin = AMIIC_SDA;				 
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP; 		 //推挽输出
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;		 //IO口速度为50MHz
	GPIO_Init(GPIOB, &GPIO_InitStructure);					 //根据设定参数初始化GPIOB.5	
}
void IIC_SDA_IN(void)//SDA输入
{
	GPIO_InitTypeDef  GPIO_InitStructure;
	GPIO_InitStructure.GPIO_Pin = AMIIC_SDA;				 
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU; 		 //上拉输入
	GPIO_Init(GPIOB, &GPIO_InitStructure);					 //根据设定参数初始化GPIOB.5	
}
//起始信号  
//SCL为高电平的时候SDA产生一个下降沿
void AMIIC_Start(void)
{
	IIC_SDA_OUT();//SDA输出
	
	IIC_SDA_H;
	IIC_SCL_H;
	delay_us(10);
	IIC_SDA_L;
	delay_us(10);
	IIC_SCL_L;
	
}
//终止信号  	
//SCL为高电平的时候SDA产生一个上升沿
void AMIIC_Stop(void)
{
	IIC_SDA_OUT();//SDA输出
	IIC_SDA_L;
	IIC_SCL_L;
	IIC_SCL_H;
	delay_us(10);
	IIC_SDA_H;
	delay_us(10);	
}
//主机应答  
//SCL为高电平的时候SDA始终为低电平
void AMIIC_ACK(void)
{
	IIC_SCL_L;
	IIC_SDA_OUT();//SDA输出
	IIC_SDA_L;
	delay_us(10);
	IIC_SCL_H;
	delay_us(10);
	IIC_SCL_L;	
}
//主机非应答  
//SCL为高电平的时候SDA始终为高电平
void AMIIC_NACK(void)
{
	IIC_SCL_L;
	IIC_SDA_OUT();//SDA输出
	IIC_SDA_H;
	delay_us(10);
	IIC_SCL_H;
	delay_us(10);
	IIC_SCL_L;	
}
//等待应答信号到来
//返回值：1，接收应答失败
//        0，接收应答成功
u8 AMIIC_Write_Ack(void)
{
	u8 tempTime=0;
	IIC_SDA_IN();//SDA输入
	IIC_SDA_H;
	delay_us(10);
	IIC_SCL_H;
	delay_us(10);
	
	while(GPIO_ReadInputDataBit(GPIO_IIC,AMIIC_SDA))
	{
		tempTime++;
		if(tempTime>250)
		{
			AMIIC_Stop();//停止信号
			return 1;
		}
	}	
	IIC_SCL_L;
	return 0;
}
//IIC发送一个字节
//返回从机有无应答
//1，有应答
//0，无应答
void AMIIC_Send_Byte(u8 txd)
{
	u8 i=0;
	IIC_SDA_OUT();//SDA输出
	IIC_SCL_L;
	for(i=0;i<8;i++)
	{
		if((txd&0x80)>0)
			IIC_SDA_H;
		else
			IIC_SDA_L;
			txd<<=1;
			IIC_SCL_H;//拉高使数据稳定传输
			delay_us(10);
			IIC_SCL_L;//拉低继续发送数据
			delay_us(10);
	}
	IIC_SCL_H;//拉高使数据稳定传输
	delay_us(10);
	IIC_SCL_L;//拉低继续发送数据
	delay_us(10);
}
//读1个字节，ack=1时，发送ACK，ack=0，发送nACK
u8 AMIIC_Read_Byte(u8 ack)
{
	u8 i=0,receive=0;
	IIC_SDA_IN();//SDA输入
	for(i=0;i<8;i++)
	{
		IIC_SCL_L;
		delay_us(10);
		IIC_SCL_H;
		delay_us(10);
		receive<<=1;
		if(GPIO_ReadInputDataBit(GPIO_IIC,AMIIC_SDA))
		{
			receive++;
		}
		delay_us(10);
	}
	if(ack==0)
		AMIIC_NACK();//主机非应答
	else
		AMIIC_ACK();//主机应答
	return receive;
}






















