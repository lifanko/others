#ifndef __MOTOR_CONTROL_H
#define __MOTOR_CONTROL_H
#include "sys.h"

#define Motor_Turntable    "M1 " //转盘电机

#define Status_Machine_Standby        0x00   //设备待机
#define Status_Machine_On             0x01   //设备开机
#define Status_Machine_Off            0x02   //设备关机
#define Status_Machine_RST            0x03   //设备复位
#define Status_Machine_Maintenance    0x04   //设备维护
#define Status_Machine_Malfunction    0x08   //设备故障中
#define Status_Machine_Selfcheck      0x10   //设备自检中
#define Status_Machine_Create_table   0x20   //图书建表中
#define Status_Machine_Borrow_Book    0x40   //借书中
#define Status_Machine_Return_Book    0x80   //还书中


#define CRLF                   "\r\n"	                 //回车换行
#define Question_Mark          "?"	                   //疑问号

#define Motor_Increment_Move   "@199:Motor:Run:"	     //增量移动    //@199:Motor:Run:M1 100	  //M1|M2|M3|M4|M5     正数正向，负数反向   0/-2
#define Motor_Absolute_Move    "@199:Motor:MoveTo:"	   //绝对移动    //@199:Motor:MoveTo:M1 100	  //M1|M2|M3|M4|M5                       0/-2

#define Motor_Run_Status       "@199:Motor:Run?"	     //马达运转状态查询 //@199:Motor:Run?	    //                                        R:10000/2  //马达1运转/错误

#define Motor_Return_Origin    "@199:Motor:Home:"	     //返回原点    //@199:Motor:Home:M1   	  //M1|M2|M3|M4|M5|ALL                      0/-2
#define Motor1_Return_Origin   "@199:Motor:Home:M1\r\n"	     //M1返回原点     0/-2
#define Motor2_Return_Origin   "@199:Motor:Home:M2\r\n"	     //M2返回原点     0/-2
#define Motor3_Return_Origin   "@199:Motor:Home:M3\r\n"	     //M3返回原点     0/-2
#define Motor4_Return_Origin   "@199:Motor:Home:M4\r\n"	     //M4返回原点     0/-2
#define Motor5_Return_Origin   "@199:Motor:Home:M5\r\n"	     //M5返回原点     0/-2

#define Motor_Return_Status    "@199:Motor:Home:"	     //返回原点状态查询//@199:Motor:Home:M1?  //M1|M2|M3|M4|M5|ALL                      H1:0/-2
#define Motor1_Return_Status    "@199:Motor:Home:M1?\r\n"	     //返回原点状态查询//                      H1:0/-2
#define Motor2_Return_Status    "@199:Motor:Home:M2?\r\n"	     //返回原点状态查询//                      H2:0/-2
#define Motor3_Return_Status    "@199:Motor:Home:M3?\r\n"	     //返回原点状态查询//                      H3:0/-2
#define Motor4_Return_Status    "@199:Motor:Home:M4?\r\n"	     //返回原点状态查询//                      H4:0/-2
#define Motor5_Return_Status    "@199:Motor:Home:M5?\r\n"	     //返回原点状态查询//                      H5:0/-2

#define Motor_Stop             "@199:Motor:Stop:"	     //马达停止    //@199:Motor:Stop:M1       //M1|M2|M3|M4|M5                          0/-2
#define Motor1_Slow_Stop        "@199:Motor:LStop:M1\r\n"	   //减速停止    //@199:Motor:LStop:M1      //M1|M2|M3|M4|M5                          0/-2

#define Motor1_Locate_Inquire  "@199:Motor:POS:M1?\r\n"	     //位置查询    //@199:Motor:POS:M1?       //M1|M2|M3|M4|M5                          0/-2
#define Motor1_Locate_Set      "@199:Motor:POS:M1 0\r\n"	     //位置更改    //@199:Motor:POS:M1 0      //M1|M2|M3|M4|M5                          0/-2

#define Motor_Speed_Set        "@199:Motor:Speed:"	   //速度设定    //@199:Motor:Speed:M1 1000 //M1|M2|M3|M4|M5      23~15000HZ          0/-2
#define Motor_Speed_Start      "@199:Motor:SStart:"	   //开始速度    //@199:Motor:SStart:M1 600 //M1|M2|M3|M4|M5      23~15000HZ          0/-2
#define Motor_Speed_Stop       "@199:Motor:SStop:"	   //停止速度    //@199:Motor:SStop:M1 600  //M1|M2|M3|M4|M5      23~15000HZ          0/-2
#define Motor_Speed_Add        "@199:Motor:SAdd:"	     //加速度      //@199:Motor:SAdd:M1 2     //M1|M2|M3|M4|M5      1~20,单位100ms      0/-2
#define Motor_Speed_Home       "@199:Motor:SHome:"	   //返回原点速度//@199:Motor:SHome:M1 100  //M1|M2|M3|M4|M5      23~15000HZ          0/-2

#define Motor_Invert_Set       "@199:Motor:Invert:"	   //反向设定    //@199:Motor:Invert:M1 1  //M1|M2|M3|M4|M5                         0/-2
#define Motor_Invert_Inquire   "@199:Motor:Invert:"	   //反向查询    //@199:Motor:Invert:M1?   //M1|M2|M3|M4|M5                         0/1 //正向/反向

#define Motor_Limit_Set        "@199:Motor:Limit:"	   //极限使能    //@199:Motor:Limit:M1 2  //M1|M2|M3|M4|M5   0：关；1：Home使能；2：均使能； 0/-2

#define Motor_EStop_Set        "@199:Motor:EStop:"	   //急停使能    //@199:Motor:EStop 1     //1|0            0：关；1：使能X8急停      0/-2

#define Motor_Active_Send_State "@199:Program:MINT"	   //状态主动发送设置//@199:Program:MINT 1     //1|0   0：关主动发送；1：开主动发送  0/-2
//M1（定位完成）   H1（返回原点完成）

#define Motor_Parameters_Save   "@199:Motor:Save"	    //保存参数    //@199:Motor:Save	  //    保存Speed Invert等参数    0/-2

#define Motor_Input_16bits    "@199:Input:"	          //IO输入查询    //@199:Input:X1?       //X1~X16或ALL                         X1:0
#define Motor_Input_2Bytes    "@199:Input:ALL2?"	    //IO输入查询    //@199:Input:ALL2?     //返回所有输入                        X:05 03

#define Motor_Count_Inquire    "@199:Count:"	          //IO计数查询    //@199:Count:C1?       //C1~C8                        计数数值
#define Motor_Count_Set        "@199:Count:"	          //IO计数修改    //@199:Count:C1 0      //C1~C8          0~65535       0/-2

#define Motor_Active_Send_XState "@199:Program:INT"	   //X状态主动发送//@199:Program:INT 1     //1|0   1：使能；0：关闭       0/-2
//X1:0    X1:1


void	Motor_Turntable_Rst(void);

#endif
