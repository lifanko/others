#include "Motor_Control.h"
#include "usart1.h"
#include "uart5.h"
#include "Host.h"
#include <stdlib.h>
#include "delay.h"
#include "stdio.h"

extern vu8 Global_Machine_Status;
extern u8  UART1_RX_BUF[USART1_REC_LEN]; //接收缓冲,最大UART4_REC_LEN个字节.末字节为换行符
extern vu8 Received_OK_UHF;
extern char Motor1_Locate_Inquire_Array[];     //马达1位置查询
extern char Motor1_Locate_Set_Array[];        //马达1位置更改

extern void Motor_Control_String(char *send, char *fun, char *Motor, int val);
extern char Motor_Increment_Move_Array[];     //增量移动发送帧

char AT_CIPSTART[50];
extern void USART1_Write(USART_TypeDef* USARTx, uint8_t *Data, uint8_t len);
extern void USART1_Clear(void);

extern vu8 Motor_CTL_Card_rst;  //电机控制卡重启标志

extern char console_val[60];

/*---------------------------------------------------------------------------------*/
//转盘电机复位 Checked by lifankohome
/*---------------------------------------------------------------------------------*/
void Motor_Turntable_Rst(void) {
	uint8_t i = 0;
	
	int steps = Get_Current_Steps();
	float grids = steps / (12000.0 / 70.0) + 1.0;
	
	sprintf(console_val, "Reset Start. Step:%d, Grid:%.2f", steps, grids);
	Log(console_val);
		
	// Reverse if already on base-point
	if((uint8_t)grids == 1) {
		steps = 12000;
	}
	
	if(steps > 6000) {
		Motor_Control_String(Motor_Increment_Move_Array, Motor_Increment_Move, Motor_Turntable, (12000-steps) + 300);
		SendCmd(Motor_Increment_Move_Array, "0", 1000);
		USART1_Clear();
		
		for (i = 0; i < 200; i++) {
			delay_ms(300);
			if ((NULL != strstr((const char *)USART1_RX_BUF, "M1"))) {
				break;
			}
		}
		delay_ms(1000);
	}

  //发送转盘电机回原点指令
  SendCmd(Motor1_Return_Origin, "0", 100);
  //转盘电机回原点状态检测
  SendCmd(Motor1_Return_Status, "H1:1", 100);
	
	Log("Reset OK");
}
