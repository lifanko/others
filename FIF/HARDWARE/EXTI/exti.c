#include "exti.h"
#include "delay.h"
#include "lock.h"
#include "key.h"
#include "usart1.h"
#include "usart2.h"
#include "Host.h"
#include "stdio.h"
#include "string.h"
#include "led.h"

extern u8 Door_Status;
extern u8 Key_Value;
extern u8 Lock_Action_Flag;//锁动作标志


//外部中断1服务程序
void EXTIX_Init(void)
{

  //每层转盘的干簧管（0：有效    1：无效）
  //开关输入：
  //*floor_1<------------->PA0
  //*floor_2<------------->PA1
  //*floor_3<------------->PA4
  //*floor_4<------------->PA5
  //*floor_5<------------->PA6
  //*floor_6<------------->PA7

  //人体感应模块<-------------------------------------------->PC2
  //门锁反馈
  //*Motor1_FB<--------------------------------------------->PC5
  //*Motor2_FB<--------------------------------->PB1
  //*Motor3_FB<--------------------------------->PB13
  //*Motor4_FB<--------------------------------->PB15
  //*Motor5_FB<--------------------------------------------->PC9
  //*Motor6_FB<------------->PA11

  EXTI_InitTypeDef EXTI_InitStructure;
  NVIC_InitTypeDef NVIC_InitStructure;
  GPIO_InitTypeDef GPIO_InitStructure;

  RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOB | RCC_APB2Periph_GPIOC, ENABLE); //使能PORTA/B/C时钟
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE); //使能复用功能时钟

  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_11 | GPIO_Pin_0 | GPIO_Pin_6 | GPIO_Pin_7 ;          //
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;     //
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;     //
  GPIO_Init(GPIOA, &GPIO_InitStructure);                //

  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_1 | GPIO_Pin_13 | GPIO_Pin_15;           //
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;     //
  GPIO_Init(GPIOB, &GPIO_InitStructure);                //

  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2 | GPIO_Pin_3 | GPIO_Pin_5 | GPIO_Pin_9;          //
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;     //
  GPIO_Init(GPIOC, &GPIO_InitStructure);

  //GPIOA.6 //顺时针按键
  GPIO_EXTILineConfig(GPIO_PortSourceGPIOC, GPIO_PinSource3);
  EXTI_InitStructure.EXTI_Line = EXTI_Line3;  //
  EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
  EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising_Falling;//上升沿和下降沿均检测，开门和关门时均上报
  EXTI_InitStructure.EXTI_LineCmd = ENABLE;
  EXTI_Init(&EXTI_InitStructure);   //

  //GPIOA.6 //逆时针按键
  GPIO_EXTILineConfig(GPIO_PortSourceGPIOA, GPIO_PinSource6);
  EXTI_InitStructure.EXTI_Line = EXTI_Line6;  //
  EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
  EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising_Falling;//上升沿和下降沿均检测，开门和关门时均上报
  EXTI_InitStructure.EXTI_LineCmd = ENABLE;
  EXTI_Init(&EXTI_InitStructure);   //

  //GPIOA.7//回原点按键
  GPIO_EXTILineConfig(GPIO_PortSourceGPIOA, GPIO_PinSource7);
  EXTI_InitStructure.EXTI_Line = EXTI_Line7;  //
  EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
  EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising_Falling;//上升沿和下降沿均检测，开门和关门时均上报
  EXTI_InitStructure.EXTI_LineCmd = ENABLE;
  EXTI_Init(&EXTI_InitStructure);   //

  //GPIOC.5 //Motor1_FB
  GPIO_EXTILineConfig(GPIO_PortSourceGPIOC, GPIO_PinSource5);
  EXTI_InitStructure.EXTI_Line = EXTI_Line5;  //
  EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
  EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising_Falling;//上升沿和下降沿均检测，开锁和关锁时均上报
  EXTI_InitStructure.EXTI_LineCmd = ENABLE;
  EXTI_Init(&EXTI_InitStructure);   //

  //GPIOB.1 //Motor2_FB
  GPIO_EXTILineConfig(GPIO_PortSourceGPIOB, GPIO_PinSource1);
  EXTI_InitStructure.EXTI_Line = EXTI_Line1;  //
  EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
  EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising_Falling;//上升沿和下降沿均检测，开锁和关锁时均上报
  EXTI_InitStructure.EXTI_LineCmd = ENABLE;
  EXTI_Init(&EXTI_InitStructure);   //

  //GPIOB.13 //Motor3_FB
  GPIO_EXTILineConfig(GPIO_PortSourceGPIOB, GPIO_PinSource13);
  EXTI_InitStructure.EXTI_Line = EXTI_Line13; //
  EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
  EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising_Falling;//上升沿和下降沿均检测，开锁和关锁时均上报
  EXTI_InitStructure.EXTI_LineCmd = ENABLE;
  EXTI_Init(&EXTI_InitStructure);   //

  //GPIOB.15 //Motor4_FB
  GPIO_EXTILineConfig(GPIO_PortSourceGPIOB, GPIO_PinSource15);
  EXTI_InitStructure.EXTI_Line = EXTI_Line15; //
  EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
  EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising_Falling;//上升沿和下降沿均检测，开锁和关锁时均上报
  EXTI_InitStructure.EXTI_LineCmd = ENABLE;
  EXTI_Init(&EXTI_InitStructure);   //

  //GPIOC.9 //Motor5_FB
  GPIO_EXTILineConfig(GPIO_PortSourceGPIOC, GPIO_PinSource9);
  EXTI_InitStructure.EXTI_Line = EXTI_Line9;  //
  EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
  EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising_Falling;//上升沿和下降沿均检测，开锁和关锁时均上报
  EXTI_InitStructure.EXTI_LineCmd = ENABLE;
  EXTI_Init(&EXTI_InitStructure);   //

  //GPIOA.11 //Motor6_FB
  GPIO_EXTILineConfig(GPIO_PortSourceGPIOA, GPIO_PinSource11);
  EXTI_InitStructure.EXTI_Line = EXTI_Line11; //
  EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
  EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising_Falling;//上升沿和下降沿均检测，开锁和关锁时均上报
  EXTI_InitStructure.EXTI_LineCmd = ENABLE;
  EXTI_Init(&EXTI_InitStructure);   //

  //GPIOC.3 //顺时针按键//GPIOA.6 //逆时针按键  //GPIOA.7//回原点按键
  NVIC_InitStructure.NVIC_IRQChannel = EXTI9_5_IRQn;      //
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x02;  //抢占优先级2，
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x03;         //子优先级3
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;               //使能外部中断通道
  NVIC_Init(&NVIC_InitStructure);

  NVIC_InitStructure.NVIC_IRQChannel = EXTI3_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x02;  //抢占优先级2，
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x03;         //子优先级3
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;               //使能外部中断通道
  NVIC_Init(&NVIC_InitStructure);

  NVIC_InitStructure.NVIC_IRQChannel = EXTI1_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x02;  //抢占优先级2，
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x03;         //子优先级3
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;               //使能外部中断通道
  NVIC_Init(&NVIC_InitStructure);

  NVIC_InitStructure.NVIC_IRQChannel = EXTI15_10_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x02;  //抢占优先级2，
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x03;         //子优先级3
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;               //使能外部中断通道
  NVIC_Init(&NVIC_InitStructure);
}

//外部中断1服务程序//*Motor2_FB
void EXTI1_IRQHandler(void)
{	
  if (EXTI_GetITStatus(EXTI_Line1) != RESET) //判断中断线上中断是否发生
  {
    Lock_Action_Flag = 1;
		
    EXTI_ClearITPendingBit(EXTI_Line1);  //清除LINE1上的中断标志位
  }
}

//外部中断2服务程序
void EXTI2_IRQHandler(void)
{
  EXTI_ClearITPendingBit(EXTI_Line2);  //清除LINE2上的中断标志位
}

//顺时针按键//PC3
void EXTI3_IRQHandler(void)
{
  if (EXTI_GetITStatus(EXTI_Line3) != RESET) //判断中断线上中断是否发生
  {
		Key_Value = KEY_CLOCKWISE ? KEY_CLOCKWISE_UP : KEY_CLOCKWISE_DOWN;
		
    EXTI_ClearITPendingBit(EXTI_Line3);  //清除LINE3上的中断标志位
  }
}

//GPIOA.6 //逆时针按键  //GPIOA.7//回原点按键
//Motor1_FB<5>   Motor5_FB<9>
void EXTI9_5_IRQHandler(void)
{
  static uint8_t reset_cnt = 0;

  if (EXTI_GetITStatus(EXTI_Line6) != RESET) //判断中断线上中断是否发生
  {
		Key_Value = KEY_ANTICLOCKWISE ? KEY_ANTICLOCKWISE_UP : KEY_ANTICLOCKWISE_DOWN;
		
    EXTI_ClearITPendingBit(EXTI_Line6);  //清除LINE6上的中断标志位
  }
  if (EXTI_GetITStatus(EXTI_Line7) != RESET) //判断中断线上中断是否发生
  {
    if (KEY_RESET == 0) //回原点
    {
      while (KEY_RESET == 0 & reset_cnt < 5) {
        reset_cnt++;

        Buzzer = 1;
        delay_ms(20);
        Buzzer = 0;
        delay_ms(180);
      }
      if (reset_cnt >= 5) {
        Key_Value = KEY_RESET_UP;
      }

      reset_cnt = 0;
    }
    EXTI_ClearITPendingBit(EXTI_Line7);  //清除LINE7上的中断标志位
  }
	
  if (EXTI_GetITStatus(EXTI_Line5) != RESET) //判断中断线上中断是否发生
  {
    Lock_Action_Flag = 1;
		
    EXTI_ClearITPendingBit(EXTI_Line5);  //清除LINE5上的中断标志位
  }

  if (EXTI_GetITStatus(EXTI_Line9) != RESET) //判断中断线上中断是否发生
  {
    Lock_Action_Flag = 1;
		
    EXTI_ClearITPendingBit(EXTI_Line9);  //清除LINE9上的中断标志位
  }
}

//定时器中断消抖

//Motor3_FB<13>   Motor4_FB<15>
//三个锁同时开2个以上，无法正常还书借书
//程序分别中断源
void EXTI15_10_IRQHandler(void)
{	
  if (EXTI_GetITStatus(EXTI_Line13) != RESET) //判断中断线上中断是否发生
  {
    Lock_Action_Flag = 1;
		
    EXTI_ClearITPendingBit(EXTI_Line13);  //清除LINE13上的中断标志位
  }

  if (EXTI_GetITStatus(EXTI_Line15) != RESET) //判断中断线上中断是否发生
  {
    Lock_Action_Flag = 1;
		
    EXTI_ClearITPendingBit(EXTI_Line15);  //清除LINE15上的中断标志位
  }
}
