#ifndef __KEY_H
#define __KEY_H	 
#include "sys.h"

#define KEY_check									GPIO_ReadInputDataBit(GPIOC,GPIO_Pin_8) // Get open button status

#define KEY_CLOCKWISE        			GPIO_ReadInputDataBit(GPIOC,GPIO_Pin_3) //读取顺时针按键
#define KEY_ANTICLOCKWISE    			GPIO_ReadInputDataBit(GPIOA,GPIO_Pin_6) //读取逆时针按键
#define KEY_RESET            			GPIO_ReadInputDataBit(GPIOA,GPIO_Pin_7) //读取回原点按键
 
#define KEY_CLOCKWISE_UP          3
#define KEY_CLOCKWISE_DOWN        4
#define KEY_ANTICLOCKWISE_UP      5
#define KEY_ANTICLOCKWISE_DOWN    6
#define KEY_RESET_UP              7
#define KEY_RESET_DOWN            8

void KEY_Init(void);//IO初始化
	    
#endif
