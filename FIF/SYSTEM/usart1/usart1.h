#ifndef __USART1_H
#define __USART1_H
#include "stdio.h"	
#include "string.h" 
#include "sys.h" 
#include "delay.h"
//////////////////////////////////////////////////////////////////////////////////	 

#define USART1_REC_LEN  			100  	//定义最大接收字节数 100
#define EN_USART1_RX 			  1		//使能（1）/禁止（0）串口1接收
#define MAX_RCV_LEN  256
	  	
extern u8  USART1_RX_BUF[USART1_REC_LEN]; //接收缓冲,最大USART1_REC_LEN个字节.末字节为换行符 
extern u8  USART1_RX_STA;         		//接收状态标记	
//如果想串口中断接收，请不要注释以下宏定义

void SendCmd(char* cmd, char* result, int timeOut);

void RS485_init(void); 	
void RS485_Send(void);    //RS485发送模式
void RS485_Received(void); //RS485接收模式
void USART1_Clear(void);
void uart1_init(u32 bound);
void USART1_Write(USART_TypeDef* USARTx, uint8_t *Data, uint8_t len);
uint32_t USART1_GetRcvNum(void);
void  USART1_GetRcvData(uint8_t *buf, uint32_t rcv_len);

#endif


