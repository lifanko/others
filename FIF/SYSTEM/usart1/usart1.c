#include "sys.h"
#include "usart1.h"
#include "uart5.h"	
#include "led.h"
////////////////////////////////////////////////////////////////////////////////// 	 
/*----------------------------------------------------------------------------*/ 	
u8 USART1_RX_BUF[USART1_REC_LEN]; //接收缓冲,最大USART1_REC_LEN个字节.
u8 USART1_RX_STA = 0;           //接收状态标记	  
vu8 Received_OK_Motor = 0;
vu8 usart1_rcv_len = 0;

//////////////////////////////////////////////////////////////////////////////////	 

void RS485_init(void)
{
	 GPIO_InitTypeDef  GPIO_InitStructure;
 	
	 RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC , ENABLE);	 //使能PC端口时钟

	 GPIO_InitStructure.GPIO_Pin = GPIO_Pin_7;	    		 	 //PC.7 端口配置, 推挽输出
	 GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP; 		 //推挽输出
	 GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;		 //IO口速度为50MHz
	 GPIO_Init(GPIOC, &GPIO_InitStructure);	  				     //推挽输出 ，IO口速度为50MHz
	
	 GPIO_ResetBits(GPIOC,GPIO_Pin_7); 				 //PC.7 输出高 
}

void RS485_Send(void)    //RS485发送模式
{
	GPIO_SetBits(GPIOC,GPIO_Pin_7); 				 
}

void RS485_Received(void) //RS485接收模式
{
	GPIO_ResetBits(GPIOC,GPIO_Pin_7); 				
}


#if EN_USART1_RX   //如果使能了接收

  
void uart1_init(u32 bound)
{
  //GPIO端口设置
	GPIO_InitTypeDef GPIO_InitStructure;
	USART_InitTypeDef USART_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1 | RCC_APB2Periph_GPIOA, ENABLE);	//使能USART1，GPIOA时钟

	//USART1_TX   GPIOA.9
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9; //PA.9
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;	//复用推挽输出
	GPIO_Init(GPIOA, &GPIO_InitStructure);//初始化GPIOA.9

	//USART1_RX	  GPIOA.10初始化
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;//PA10
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;//浮空输入
	GPIO_Init(GPIOA, &GPIO_InitStructure);//初始化GPIOA.10  

	//USART1 NVIC 配置
	NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority=2 ;//抢占优先级2
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 3;		//子优先级3
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;			//IRQ通道使能
	NVIC_Init(&NVIC_InitStructure);	//根据指定的参数初始化VIC寄存器

	//USART1 初始化设置

	USART_InitStructure.USART_BaudRate = bound;//串口波特率
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;//字长为8位数据格式
	USART_InitStructure.USART_StopBits = USART_StopBits_1;//一个停止位
	USART_InitStructure.USART_Parity = USART_Parity_No;//无奇偶校验位
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;//无硬件数据流控制
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;	//收发模式

	USART_Init(USART1, &USART_InitStructure); //初始化串口1
	USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);//开启串口接受中断
	USART_Cmd(USART1, ENABLE);                    //使能串口1 
}

#endif	


void USART1_IRQHandler(void)
{
    unsigned int data;
#if 1
    if(USART1->SR & 0x0F)
    {
        // See if we have some kind of error
        // Clear interrupt (do nothing about it!)
        data = USART1->DR;
    }
    else if(USART1->SR & USART_FLAG_RXNE)   //Receive Data Reg Full Flag
    {

			data = USART1->DR;
			USART1_RX_BUF[usart1_rcv_len++] = data;
			if(usart1_rcv_len >= MAX_RCV_LEN - 1)
				usart1_rcv_len = 0;
					//usart1_rcv_buf[usart1_rcv_len++]=data;
					//usart1_putrxchar(data);       //Insert received character into buffer
    }
    else
    {
        ;
    }
#endif
}

/*
*  @brief USART1串口接收状态初始化
*/
void USART1_Clear(void)
{
		memset(USART1_RX_BUF, 0, sizeof(USART1_RX_BUF));
    usart1_rcv_len = 0;
}

/*
*  @brief USART1串口发送api
*/
void USART1_Write(USART_TypeDef* USARTx, uint8_t *Data, uint8_t len)
{
    uint8_t i;
    USART_ClearFlag(USARTx, USART_FLAG_TC);
    for(i = 0; i < len; i++)
    {
        USART_SendData(USARTx, *Data++);
        while( USART_GetFlagStatus(USARTx, USART_FLAG_TC) == RESET );
    }
}

/*
 *  @brief USART1串口发送AT命令用
 *  @para  cmd  AT命令
 *  @para  result 预期的正确返回信息
 *  @para  timeOut延时时间,ms
 */
void SendCmd(char* cmd, char* result, int timeOut)
{
	while(1) {
		USART1_Clear();
		
		USART1_Write(USART1, (unsigned char *)cmd, strlen((const char *)cmd));
		delay_ms(timeOut);
		
		if(DEBUG || (NULL != strstr((const char *)USART1_RX_BUF, result))) {	//判断是否有预期的结果
			break;
		} else {
			delay_ms(95);
			Buzzer = 1;
			delay_ms(5);
			Buzzer = 0;
		}
	}
}


#if 1
/*
 *  @brief 返回USART1已接收的数据长度
 */
uint32_t USART1_GetRcvNum(void)
{
    return usart1_rcv_len;
}

/*
 *  @brief 返回USART1已接收的数据到buf，长度为rcv_len
 */
void  USART1_GetRcvData(uint8_t *buf, uint32_t rcv_len)
{
    if(buf)
    {
        memcpy(buf, USART1_RX_BUF, rcv_len);
    }
    USART1_Clear();
}
#endif
