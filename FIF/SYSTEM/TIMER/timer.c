#include "timer.h"
#include "led.h"
#include "wdg.h"
#include "dma.h"
#include "delay.h"

extern u8 Time_Temp_Hum;
extern u8 Second;
extern u8 Minute ;
extern u8 Hour;

extern vu8 Fault_Code_Refresh;//故障代码刷新标志

// Watch dog block flag
extern u8 block;

extern u8 send_close;

extern vu8 Lock_Action_Flag;

//通用定时器中断初始化
//这里时钟选择为APB1的2倍，而APB1为36M
//arr：自动重装值。
//psc：时钟预分频数
//这里使用的是定时器3!
void TIM3_Int_Init(u16 arr, u16 psc)
{
  TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
  NVIC_InitTypeDef NVIC_InitStructure;

  RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE); //时钟使能

  TIM_TimeBaseStructure.TIM_Period = arr; //设置在下一个更新事件装入活动的自动重装载寄存器周期的值   计数到5000为500ms
  TIM_TimeBaseStructure.TIM_Prescaler = psc; //设置用来作为TIMx时钟频率除数的预分频值  10Khz的计数频率
  TIM_TimeBaseStructure.TIM_ClockDivision = 0; //设置时钟分割:TDTS = Tck_tim
  TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;  //TIM向上计数模式
  TIM_TimeBaseInit(TIM3, &TIM_TimeBaseStructure); //根据TIM_TimeBaseInitStruct中指定的参数初始化TIMx的时间基数单位

  TIM_ITConfig(TIM3, TIM_IT_Update , ENABLE);

  NVIC_InitStructure.NVIC_IRQChannel = TIM3_IRQn;  //TIM3中断
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;  //先占优先级3级
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 2;  //从优先级3级
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE; //IRQ通道被使能
  NVIC_Init(&NVIC_InitStructure);  //根据NVIC_InitStruct中指定的参数初始化外设NVIC寄存器

  TIM_Cmd(TIM3, ENABLE);  //禁止TIMx外设

}

void TIM3_IRQHandler(void)   //TIM3中断, 1s
{
  static u8 second_counter = 0;
  
  static u8 send_close_delay = 0;

  if (TIM_GetITStatus(TIM3, TIM_IT_Update) != RESET) //检查指定的TIM中断发生与否:TIM 中断源
  {
    SYS_RUN = ~SYS_RUN;
		
    if (block) {
			Buzzer = 1;
			delay_ms(50);
			Buzzer = 0;
    } else {
			IWDG_Feed();//喂狗
      
      if(send_close){
        if(++send_close_delay == 6) {
          send_close_delay = 0;
          send_close = 0;
          
          Lock_Action_Flag = 0x54;
        }
      }
		}

		if (++Second == 60) {
			Second = 0;

			if (++Minute == 60) {
				Minute = 0;

				// MAX: 65536
				Hour++;
			}
		}

		if (++second_counter == 10) {
			Fault_Code_Refresh = 1;
		}

    TIM_ClearITPendingBit(TIM3, TIM_IT_Update);  //清除TIMx的中断待处理位:TIM 中断源
  }
}
