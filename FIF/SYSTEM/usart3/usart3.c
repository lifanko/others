#include "sys.h"
#include "usart3.h"	  
#include "delay.h"

#include "stdio.h"	
#include "string.h" 


 

#if EN_USART3_RX   //如果使能了接收
/*----------------------------------------------------------------------------*/ 	
u8 USART3_RX_BUF[USART3_REC_LEN]; //接收缓冲,最大USART_REC_LEN个字节.
u8 Received_OK_Divider;//天线分频器响应标志位
vu8 USART3_RX_Counter = 0;
/*----------------------------------------------------------------------------*/ 

void uart3_init(u32 bound)
{
  //GPIO端口设置
  GPIO_InitTypeDef GPIO_InitStructure;
	USART_InitTypeDef USART_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;
	 
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);	//GPIOB时钟
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART3, ENABLE);	//使能USART3时钟

	//USART3_TX   GPIOB.10
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10; //PB.10
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;	//复用推挽输出
  GPIO_Init(GPIOB, &GPIO_InitStructure);//初始化GPIOB.10
   
  //USART3_RX	  GPIOB.11初始化
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_3;//PB11
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;//浮空输入
  GPIO_Init(GPIOB, &GPIO_InitStructure);//初始化GPIOB.11

  //USART3 NVIC 配置
  NVIC_InitStructure.NVIC_IRQChannel = USART3_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1 ;//抢占优先级1
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 3;		//子优先级3
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;			//IRQ通道使能
	NVIC_Init(&NVIC_InitStructure);	//根据指定的参数初始化VIC寄存器
  
   //USART 初始化设置

	USART_InitStructure.USART_BaudRate = bound;  //串口波特率
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;  //字长为8位数据格式
	USART_InitStructure.USART_StopBits = USART_StopBits_1;  //一个停止位
	USART_InitStructure.USART_Parity = USART_Parity_No;     //无奇偶校验位
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;//无硬件数据流控制
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;	//收发模式

  USART_Init(USART3, &USART_InitStructure);   //初始化串口3
	
  USART_ITConfig(USART3, USART_IT_RXNE, ENABLE);  //开启接收中断
	USART_ITConfig(USART3, USART_IT_IDLE, ENABLE);  //开启空闲中断
	
  USART_Cmd(USART3, ENABLE);     //使能串口3

}

void USART3_IRQHandler(void)   	//串口3中断服务程序
{
	u8 clear = clear;
	if(USART_GetITStatus(USART3, USART_IT_RXNE) != RESET)
	{
		USART3_RX_BUF[USART3_RX_Counter++] =  USART3->DR;
	}
	else if(USART_GetITStatus(USART3, USART_IT_IDLE) != RESET)
	{
		clear = USART3->SR;
		clear = USART3->DR;
		Received_OK_Divider = 1;
	}
} 


//void USART3_IRQHandler(void)   	//串口2中断服务程序
//{
//	if(USART_GetITStatus(USART3, USART_IT_IDLE) != RESET)  //空闲中断
//		{
//			DMA_Cmd(DMA1_Channel6, DISABLE);	//关闭DMA功能
//			
//			if(0xAB == USART3_RX_BUF[0])//判断帧头
//			{
//				Received_OK_RS232 = 1;	
//			}
////			else
////				memset( USART3_RX_BUF,'\0',sizeof(USART3_RX_BUF));//memset（）清空数组，效率最高
////	
//			USART_ClearITPendingBit(USART3, USART_IT_IDLE);//无法清除空闲中断
//			DMA_Cmd(DMA1_Channel6, ENABLE);
//     } 
//} 


#endif	



