#include "sys.h"
#include "uart4.h"	  
////////////////////////////////////////////////////////////////////////////////// 	 

#if EN_UART4_RX   //如果使能了接收
//串口1中断服务程序
//注意,读取USARTx->SR能避免莫名其妙的错误   	
u8 UART4_RX_BUF[UART4_REC_LEN];     //接收缓冲,最大USART_REC_LEN个字节.
//接收状态
//bit15，	接收完成标志
//bit14，	接收到0x0d
//bit13~0，	接收到的有效字节数目
u16 UART4_RX_STA=0;       //接收状态标记	

vu8 Received_OK_UHF = 0; 
vu8 UART4_RX_Counter = 0;

void uart4_init(u32 bound)
{
  //GPIO端口设置
  GPIO_InitTypeDef GPIO_InitStructure;
	USART_InitTypeDef USART_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;
	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);	//使能GPIOC时钟
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_UART4, ENABLE);	//使能UART4时钟
  
	//UART4_TX   GPIOC.10
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10; //PC10
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;	//复用推挽输出
  GPIO_Init(GPIOC, &GPIO_InitStructure);//初始化GPIOC.10
   
  //UART4_RX	  GPIOC.11初始化
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_11;//PC11
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;//上拉输入，浮空输入有干扰！！！！
  GPIO_Init(GPIOC, &GPIO_InitStructure);//初始化GPIOC.11  

  //Uart4 NVIC 配置
  NVIC_InitStructure.NVIC_IRQChannel = UART4_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority=1 ;//抢占优先级3
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 3;		//子优先级3
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;			//IRQ通道使能
	NVIC_Init(&NVIC_InitStructure);	//根据指定的参数初始化VIC寄存器
  
   //USART 初始化设置

	USART_InitStructure.USART_BaudRate = bound;//串口波特率
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;//字长为8位数据格式
	USART_InitStructure.USART_StopBits = USART_StopBits_1;//一个停止位
	USART_InitStructure.USART_Parity = USART_Parity_No;//无奇偶校验位
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;//无硬件数据流控制
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;	//收发模式

  USART_Init(UART4, &USART_InitStructure); //初始化串口4
	
	USART_ITConfig(UART4, USART_IT_RXNE, ENABLE);//开启串口空闲中断
  USART_ITConfig(UART4, USART_IT_IDLE, ENABLE);//开启串口空闲中断
	
  USART_Cmd(UART4, ENABLE);                    //使能串口4 

}


//void UART4_IRQHandler(void)                	//串口4中断服务程序
//{

//	if(USART_GetITStatus(UART4, USART_IT_IDLE) != RESET)  //空闲中断
//		{
//			DMA_Cmd(DMA2_Channel3, DISABLE);	//关闭DMA功能
//			
//			if(0xBB == UART4_RX_BUF[0])//判断帧头
//			{
//				Received_OK_UHF = 1;	
//			}				
//			USART_ClearITPendingBit(UART4, USART_IT_IDLE);
//			DMA_Cmd(DMA2_Channel3, ENABLE);
//     } 
//} 

void UART4_IRQHandler(void)   	//串口4中断服务程序
{
	u8 clear = clear;
	if(USART_GetITStatus(UART4, USART_IT_RXNE) != RESET)
	{
		UART4_RX_BUF[UART4_RX_Counter++] =  UART4->DR;
	}
	else if(USART_GetITStatus(UART4, USART_IT_IDLE) != RESET)
	{
		clear = UART4->SR;
		clear = UART4->DR;
		Received_OK_UHF = 1;
	}
} 


#endif	


/*
void UART4_Config(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;             
	USART_InitTypeDef USART_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;
	DMA_InitTypeDef DMA_InitStructure;	 //定义DMA初始化结构体DMA_InitStructure 
	    
//	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_0); 	 //选择NVIC优先级分组0  
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_UART4, ENABLE);
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA2, ENABLE);

	//串口4所使用管脚输出输入定义
	
	//定义UART4 Tx (PC.10)脚为复用推挽输出
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;         //IO口的第2脚
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz; //IO口速度
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;   //IO口复用推挽输出
	GPIO_Init(GPIOC, &GPIO_InitStructure);            //初始化串口1输出IO口
	
	//定义 UART4 Rx (PC.11)为悬空输入 
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_11;           //IO口的第3脚
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;//IO口悬空输入
	GPIO_Init(GPIOC, &GPIO_InitStructure);               //初始化串口1输入IO口

	//串口4参数初始化定义部分,串口1参数为9600 ， 8 ，1 ，N  接收中断方式  
	USART_InitStructure.USART_BaudRate = 9600;                  //设定传输速率
	USART_InitStructure.USART_WordLength = USART_WordLength_8b; //设定传输数据位数
	USART_InitStructure.USART_StopBits = USART_StopBits_1;      //设定停止位个数
	USART_InitStructure.USART_Parity = USART_Parity_No ;        //不用校验位
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;//不用流量控制
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;                //使用接收和发送功能	
	USART_Init(UART4, &USART_InitStructure);      //初始化串口4
	
	USART_ITConfig(UART4, USART_IT_IDLE,ENABLE);  //使能串口4接收中断	
	USART_Cmd(UART4, ENABLE);                     //使能串口4	
	USART_ClearFlag(UART4, USART_FLAG_TC);        // 清标志(后增加)

	DMA_DeInit(DMA2_Channel3);	 //重置DMA 2通道配置	
	DMA_InitStructure.DMA_PeripheralBaseAddr = 0x40004C04;	 //外设地址  
	DMA_InitStructure.DMA_MemoryBaseAddr = (u32)UART4_Rx_buffer;	 //内存地址  
	DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralSRC;	 //外设作为数据目的地 
	DMA_InitStructure.DMA_BufferSize = 512;	 //DMA缓存大小:BufferSize 
	DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;	 //外设地址寄存器不递增  
	DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;	  	 //内存地址寄存器递增	
	DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte; 	//外设数据宽度为8位 
	DMA_InitStructure.DMA_MemoryDataSize = DMA_PeripheralDataSize_Byte;	 //内存数据宽度为8位 
	DMA_InitStructure.DMA_Mode = DMA_Mode_Circular;	 //工作在正常缓存模式 
	DMA_InitStructure.DMA_Priority = DMA_Priority_VeryHigh;	 //设置DMA通道优先级为高 
	DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;	 //禁止DMA通道设置为内存至内存传输 
	DMA_Init(DMA2_Channel3, &DMA_InitStructure);	 //初始化
	  
 	DMA_ITConfig(DMA2_Channel3, DMA_IT_TC, ENABLE);
	DMA_ITConfig(DMA2_Channel3, DMA_IT_TE, ENABLE);

	USART_DMACmd(UART4, USART_DMAReq_Rx, ENABLE);
	DMA_Cmd(DMA2_Channel3, ENABLE);
	//使能串口4中断
	NVIC_InitStructure.NVIC_IRQChannel = UART4_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);	

	NVIC_InitStructure.NVIC_IRQChannel = DMA2_Channel3_IRQn; 
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 2; 
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0; 
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE; 
	NVIC_Init(&NVIC_InitStructure);
 }

*/


