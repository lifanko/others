#include "sys.h"
#include "usart2.h"	  
#include "delay.h"

#include "stdio.h"	
#include "string.h" 

extern vu8 Read_Status;
/*-----------------------------------------------------------------------*/
//extern void Machine_Control_Handle(void);
/*----------------------------------------------------------------------------*/

#if EN_USART2_RX   //如果使能了接收
/*----------------------------------------------------------------------------*/ 	
u8 USART2_RX_BUF[USART2_REC_LEN]; //接收缓冲,最大USART_REC_LEN个字节.
vu8 Received_OK_RS232 = 0;
vu8 Received_OK_Machine_Status = 0;
vu8 USART2_RX_Counter = 0;
/*----------------------------------------------------------------------------*/ 

void uart2_init(u32 bound)
{
  //GPIO端口设置
  GPIO_InitTypeDef GPIO_InitStructure;
	USART_InitTypeDef USART_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;
	 
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);	//GPIOA时钟
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE);	//使能USART2时钟

	//USART2_TX   GPIOA.2
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2; //PA.2
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;	//复用推挽输出
  GPIO_Init(GPIOA, &GPIO_InitStructure);//初始化GPIOA.2
   
  //USART2_RX	  GPIOA.3初始化
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_3;//PA3
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;//浮空输入
  GPIO_Init(GPIOA, &GPIO_InitStructure);//初始化GPIOA.3 

  //USART2 NVIC 配置
  NVIC_InitStructure.NVIC_IRQChannel = USART2_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority=0 ;//抢占优先级1
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 3;		//子优先级3
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;			//IRQ通道使能
	NVIC_Init(&NVIC_InitStructure);	//根据指定的参数初始化VIC寄存器
  
   //USART 初始化设置
	USART_InitStructure.USART_BaudRate = bound;  //串口波特率
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;  //字长为8位数据格式
	USART_InitStructure.USART_StopBits = USART_StopBits_1;  //一个停止位
	USART_InitStructure.USART_Parity = USART_Parity_No;     //无奇偶校验位
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;//无硬件数据流控制
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;	//收发模式

  USART_Init(USART2, &USART_InitStructure);   //初始化串口2
	
  USART_ITConfig(USART2, USART_IT_RXNE, ENABLE);  //开启接收中断
	USART_ITConfig(USART2, USART_IT_IDLE, ENABLE);  //开启空闲中断
	
  USART_Cmd(USART2, ENABLE);     //使能串口2 

}

void USART2_IRQHandler(void)   	//串口2中断服务程序
{
	if(USART_GetITStatus(USART2, USART_IT_RXNE) != RESET)
	{
		USART2_RX_BUF[USART2_RX_Counter++] =  USART2->DR;
		if(USART2_RX_Counter > 30)
		{
			USART2_RX_Counter = 0;
		}
	}
	if(USART_GetITStatus(USART2, USART_IT_IDLE) != RESET)
	{
		USART2->SR;
		USART2->DR;
		Received_OK_RS232 = 1;
	}
}


#endif	

/*--------------------------USART 发送一个字节---------------------------------*/

void USART_SendData_User(USART_TypeDef* USART_Ch,u8 data)
{
	USART_SendData(USART_Ch,data);
	while(USART_GetFlagStatus(USART_Ch,USART_FLAG_TC) != SET);
}

/*---------------------------USART 发送一帧数据----------------------------------*/

void USART_SendString_User(USART_TypeDef* USART_Ch,u8 *p,u8 StringLen)
{
	u8 i;
	USART_ClearFlag(USART_Ch, USART_FLAG_TC);
	for(i = 0;i < StringLen;i++)
	{
		USART_SendData_User(USART_Ch,*(p + i));
	}
}	

