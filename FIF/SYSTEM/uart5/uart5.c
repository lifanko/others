#include "sys.h"
#include "uart5.h"
#include "string.h"
#include "delay.h"
//////////////////////////////////////////////////////////////////////////////////

#if EN_UART5_RX   //如果使能了接收

void uart5_init(u32 bound)
{
  //GPIO端口设置
  GPIO_InitTypeDef GPIO_InitStructure;
  USART_InitTypeDef USART_InitStructure;
  NVIC_InitTypeDef NVIC_InitStructure;

  RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE); //使能GPIOC时钟
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_UART5, ENABLE); //使能UART5时钟

  //UART5_TX   GPIOC.12
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_12; //PC12
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP; //复用推挽输出
  GPIO_Init(GPIOC, &GPIO_InitStructure);//初始化GPIOC.10

  //UART5 NVIC 配置
  NVIC_InitStructure.NVIC_IRQChannel = UART5_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1 ; //抢占优先级3
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 3;    //子优先级3
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;     //IRQ通道使能
  NVIC_Init(&NVIC_InitStructure); //根据指定的参数初始化VIC寄存器

  //USART 初始化设置

  USART_InitStructure.USART_BaudRate = bound;//串口波特率
  USART_InitStructure.USART_WordLength = USART_WordLength_8b;//字长为8位数据格式
  USART_InitStructure.USART_StopBits = USART_StopBits_1;//一个停止位
  USART_InitStructure.USART_Parity = USART_Parity_No;//无奇偶校验位
  USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;//无硬件数据流控制
  USART_InitStructure.USART_Mode = USART_Mode_Tx; //收发模式

  USART_Init(UART5, &USART_InitStructure); //初始化串口5

  USART_Cmd(UART5, ENABLE);                //使能串口5
}


void Log(char *Data) {
	uint8_t i;
	uint8_t len = strlen(Data);
	
	USART_ClearFlag(UART5, USART_FLAG_TC);
	for(i = 0; i < len; i++) {
		USART_SendData(UART5, *Data++);
		while(USART_GetFlagStatus(UART5, USART_FLAG_TC) == RESET);
	}
	
	USART_SendData(UART5, '\n');
	while(USART_GetFlagStatus(UART5, USART_FLAG_TC) == RESET);
	
	delay_ms(20);
}

#endif
