//参数说明：参数1-三角形大小（百分比）；参数2-三角形填充字符

var fs = require("fs");

var str = fs.readFileSync('string.txt', 'utf8'); //读取配置文件

//去除文本换行符号
str = str.replace(/\r|\n/g, "");

var triangleSize = 0.6;     //三角形默认大小为60%
var triangleChar = ' ';     //三角形填充默认为空格

var args = process.argv.splice(2);
if (args[0] !== undefined) {
    triangleSize = args[0];
}
if (args[1] !== undefined) {
    triangleChar = args[1];
}

var fill = ' ';     //字符后间隔，默认为空格

var squareWidth = 0;
var squareHeight = 0;

//根据字符串长度确定绘图区大小，因为三角形空格会填充一部分，所以绘图边长使用向上取整（！！！三角形范围为1时很有可能超出区域！！！）
var square = Math.ceil(Math.sqrt(str.length));

//为了让三角形居中显示，如果绘图边长是奇数则继续，是偶数则为绘图区宽度加1
if (square % 2 === 1) {
    squareWidth = squareHeight = square;
} else {
    console.log('Resize');
    squareWidth = square + 1;
    //根据绘图区宽度计算新的绘图区高度（同样向上取整）
    squareHeight = Math.ceil(str.length / squareWidth);
}

console.log('String Length: ' + str.length + ' PaperWidth: ' + squareWidth + ' PaperHeight: ' + squareHeight + ' PaperSize: ' + squareWidth * squareHeight);

//三角形大小可定义，范围：50%-100%
//判断输入是否合法
if (!(triangleSize >= 0.5 && triangleSize <= 1)) {
    console.log('Illegal Triangle Range!');
} else {
    triangleSize = Math.floor(squareWidth * triangleSize);
    if (triangleSize % 2 !== 1) {
        triangleSize = triangleSize - 1;
    }

    //三角形高度使用四舍五入法
    var triangleHeight = Math.round(triangleSize / Math.sqrt(3));
    console.log('Triangle Width: ' + triangleSize + ' Triangle Height: ' + triangleHeight);

    var triangleStartX = (squareWidth - triangleSize) / 2;
    //根据三角形大小确定三角形高度和在绘图区的起始高度
    var triangleStartY = Math.ceil((squareHeight - triangleHeight) / 2);
    console.log('TriangleStartX: ' + triangleStartX + ' TriangleStartY: ' + triangleStartY);

    var i;
    var j = 0;
    var result = '';
    var index = 0;
    var spaceNum = 0;

    //绘图区
    for (i = 0; i < squareWidth + 1; i++) {
        result += '--'
    }
    console.log(result);

    //上无三角形区域
    for (i = 0; i < triangleStartY; i++) {
        result = '';

        for (j = 0; j < squareWidth; j++) {
            result += str.charAt(index++) + fill;
        }

        if (i < 10) {
            console.log(result + '0' + i);
        } else {
            console.log(result + i);
        }
    }

    //中间有三角形区域
    var middle = (squareWidth - 1) / 2;
    for (i = 0; i < triangleHeight; i++) {
        result = '';
        for (j = 0; j < squareWidth; j++) {

            if (Math.abs(middle - j) === i) {
                result += triangleChar + fill;
                spaceNum++;
            } else {
                if (i + 1 === triangleHeight && Math.abs(middle - j) < i) {
                    result += triangleChar + fill;
                    spaceNum++;
                } else {
                    result += str.charAt(index++) + fill;
                }
            }
        }
        if (i < 10) {
            console.log(result + '0' + i);
        } else {
            console.log(result + i);
        }
    }

    //下部无三角形区域
    for (i = 0; i < squareHeight - triangleStartY - triangleHeight; i++) {
        result = '';

        for (j = 0; j < squareWidth; j++) {
            var char = str.charAt(index++);

            //字符若不足则补空格（为了对齐标号）
            if (char) {
                result += char + fill;
            } else {
                result += '  ';
            }
        }

        if (i < 10) {
            console.log(result + '0' + i);
        } else {
            console.log(result + i);
        }
    }

    //序号
    result = '';
    for (i = 0; i < squareWidth; i++) {
        result += i % 10 + fill;
    }
    console.log(result);

    //绘图区
    result = '';
    for (i = 0; i < squareWidth + 1; i++) {
        result += '--'
    }
    console.log(result);

    //输出绘图区添加的空格数量
    console.log('Triangle Char Number: ' + spaceNum);
}
